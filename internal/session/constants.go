package session

import "github.com/lestrrat-go/jwx/jwa"

type ValidateKey string

const (
	AccountUUIDClaimKey                  = "uid"
	DefaultCookieID                      = "codeprac"
	DefaultCookieDomain                  = "localhost"
	DefaultCookiePath                    = "/"
	DefaultSessionContextKey             = "session"
	DefaultTokenSubject                  = "https://codepr.ac"
	DefaultTokenIssuer                   = "codeprac"
	DefaultTokenAudience                 = "codeprac.account"
	DefaultTokenAlgorithm                = jwa.RS512
	SessionTokenClaimKey                 = "tok"
	TokenLength                          = 48
	ValidateKeyToken         ValidateKey = "token"
)
