package session

import (
	"errors"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// CreateSessionOpts provides a structure for the CreateSession method arguments
type CreateSessionOpts struct {
	DatabaseConnection *gorm.DB
	AccountUUID        string
	UserAgent          *string
	IPAddress          *string
}

// Validate returns an error if any required parameters are missing/invalid
func (o CreateSessionOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(o.AccountUUID) == 0 {
		errors = append(errors, "missing account uuid")
	} else if _, err := uuid.Parse(o.AccountUUID); err != nil {
		errors = append(errors, "invalid account uuid")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for creating a session: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

// CreateSession inserts a new session into the database
func CreateSession(opts CreateSessionOpts) (*Session, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to create session object: %s", err)
	}
	db := opts.DatabaseConnection
	sessionInstance := Session{
		AccountUUID: opts.AccountUUID,
		Token:       generateSessionToken(TokenLength),
		UserAgent:   opts.UserAgent,
		IPAddress:   opts.IPAddress,
	}
	if query := db.Create(&sessionInstance); query.Error != nil {
		return nil, fmt.Errorf("failed to insert session into database: %s", query.Error)
	}
	return &sessionInstance, nil
}

// GetSessionOpts provides a structure for the GetSession method arguments
type GetSessionOpts struct {
	DatabaseConnection *gorm.DB
	Session            *Session
}

// Validate returns an error if any required parameters are missing/invalid
func (o GetSessionOpts) Validate(keys ...ValidateKey) error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(keys) > 0 {
		for _, key := range keys {
			switch key {
			case ValidateKeyToken:
				if len(o.Session.Token) == 0 {
					errors = append(errors, "missing session token")
				}
			}
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for getting a session: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

// GetSessionByToken retrieves the session information given a session token
func GetSessionByToken(opts GetSessionOpts) (*Session, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to get session by token: %s", err)
	}
	sessionInstance := Session{Token: opts.Session.Token}
	if query := opts.DatabaseConnection.Where(sessionInstance).First(&sessionInstance); query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to get session from database: %s", query.Error)
	}
	return &sessionInstance, nil
}

// UpdateSessionOpts provides a structure for the UpdateSession method arguments
type UpdateSessionOpts struct {
	DatabaseConnection *gorm.DB
	Session            *Session
}

// Validate returns an error if any required parameters are missing/invalid
func (o UpdateSessionOpts) Validate(keys ...ValidateKey) error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.Session == nil {
		errors = append(errors, "missing session instance")
	} else if o.Session.ID == 0 {
		errors = append(errors, "missing session id")
	}

	if len(keys) > 0 {
		for _, key := range keys {
			switch key {
			case ValidateKeyToken:
				if len(o.Session.Token) == 0 {
					errors = append(errors, "missing session token")
				}
			}
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for updating a session: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

// UpdateSession updates the session instance in the databaase with the provided
// Session object in the parameters
func UpdateSession(opts UpdateSessionOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to update session: %s", err)
	}
	if query := opts.DatabaseConnection.UpdateColumns(opts.Session); query.Error != nil {
		return fmt.Errorf("failed to update session from database: %s", query.Error)
	}
	return nil
}
