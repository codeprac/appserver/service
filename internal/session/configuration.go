package session

import "github.com/usvc/go-config"

const (
	ConfigKeySigningKey         = "session-signing-key"
	ConfigKeySigningKeyPassword = "session-signing-key-password"
	ConfigKeySigningKeyPath     = "session-signing-key-path"
	ConfigKeyVerifyingKey       = "session-verifying-key"
	ConfigKeyVerifyingKeyPath   = "session-verifying-key-path"
)

func GetConfiguration() config.Map {
	return config.Map{
		ConfigKeySigningKeyPath: &config.String{
			Usage: "path to the rsa private key to use for signing session tokens",
		},
		ConfigKeySigningKeyPassword: &config.String{
			Usage: "password of the rsa private key to use for signing session tokens (if applicable only)",
		},
		ConfigKeySigningKey: &config.String{
			Usage: "base64-encoded rsa private key to use for signing session tokens",
		},
		ConfigKeyVerifyingKeyPath: &config.String{
			Usage: "path to the rsa public key to use for verifying session tokens",
		},
		ConfigKeyVerifyingKey: &config.String{
			Usage: "base64-encoded rsa public key to use for verifying session tokens",
		},
	}
}
