package session

import (
	"crypto/rsa"
	"fmt"
	"time"

	"github.com/lestrrat-go/jwx/jwt"
	"gitlab.com/codeprac/codeprac/internal/account"
	"gorm.io/gorm"
)

type Session struct {
	gorm.Model
	ID          uint             `json:"id"`
	UUID        string           `json:"uuid" gorm:"unique;type:uuid;default:uuid_generate_v4()"`
	AccountUUID string           `json:"accountUUID" gorm:"type:uuid"`
	Account     *account.Account `json:"account" gorm:"references:UUID"`
	Token       string           `json:"token"`
	UserAgent   *string          `json:"userAgent"`
	IPAddress   *string          `json:"ipAddress"`
}

// GetToken returns a JWT token based on this Session instance's data and mainly
// addresses the question of "how can we identify a user?"
func (s Session) GetToken(signingKey rsa.PrivateKey) ([]byte, error) {
	clientToken := jwt.New()
	clientToken.Set(jwt.SubjectKey, DefaultTokenSubject)
	clientToken.Set(jwt.IssuerKey, DefaultTokenIssuer)
	clientToken.Set(jwt.AudienceKey, DefaultTokenAudience)
	clientToken.Set(jwt.IssuedAtKey, time.Now())
	clientToken.Set(SessionTokenClaimKey, s.Token)
	clientToken.Set(AccountUUIDClaimKey, s.AccountUUID)
	signedClientToken, err := jwt.Sign(clientToken, DefaultTokenAlgorithm, signingKey)
	if err != nil {
		return nil, fmt.Errorf("failed to sign jwt token: %s", err)
	}
	return signedClientToken, nil
}
