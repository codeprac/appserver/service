package session

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

type UtilsTests struct {
	suite.Suite
}

func TestUtils(t *testing.T) {
	suite.Run(t, &UtilsTests{})
}

func (s UtilsTests) Test_generateSessionToken() {
	token1 := generateSessionToken(16)
	token2 := generateSessionToken(16)
	s.Len(token1, 16)
	s.Len(token2, 16)
	log.Info(token1)
	log.Info(token2)
	s.NotEqualValues(token1, token2)
}
