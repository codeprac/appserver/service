package session

import (
	"context"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"

	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
	"gitlab.com/codeprac/codeprac/internal/constants"
)

// generateSessionToken essentially creates a random string of provided length
func generateSessionToken(length uint) string {
	sessionToken := make([]byte, length)
	// without this, all tokens generated will be the same based on order of generation
	// rand.Seed(time.Now().Unix())
	for i := range sessionToken {
		sessionToken[i] = constants.TokenCharacters[rand.Intn(len(constants.TokenCharacters))]
	}
	return string(sessionToken)
}

// GetSessionInfoFromRequest is used in request handlers to extract session information
// from the request context, this should be used in conjunction with a token provider
// server middleware that injects the session information into the incoming request
func GetSessionInfoFromRequest(r *http.Request) (*TokenData, error) {
	sessionContext := r.Context().Value(DefaultSessionContextKey)
	sessionData, ok := sessionContext.(map[string]interface{})
	if !ok {
		return nil, errors.New("failed to get valid session data from request context")
	}
	sessionToken := TokenData(sessionData)
	return &sessionToken, nil
}

type TokenData map[string]interface{}

func (td TokenData) GetAccountUUID() string {
	if accountUUIDValue, ok := td[AccountUUIDClaimKey]; ok {
		if accountUUID, ok := accountUUIDValue.(string); ok {
			return accountUUID
		}
	}
	return ""
}

func (td TokenData) GetSessionToken() string {
	if sessionTokenValue, ok := td[SessionTokenClaimKey]; ok {
		if sessionToken, ok := sessionTokenValue.(string); ok {
			return sessionToken
		}
	}
	return ""
}

// GetSignVerifyTokenPairOpts is the option set for the GetSignVerifyTokenPair method
type GetSignVerifyTokenPairOpts struct {
	SigningKeyBase64   string
	SigningKeyPassword string
	SigningKeyPath     string
	VerifyingKeyBase64 string
	VerifyingKeyPath   string
}

// GetSignVerifyTokenPair does the work of generating an application-understandable
// private and public key pair that can be used for signing and verifying tokens.
// The base64-encoded variant is prioritised, so if that is specified, it will be
// used over the path to a physical file. If your private key is protected by a
// password, remember to specify that in the options as well
func GetSignVerifyTokenPair(opts GetSignVerifyTokenPairOpts) (*rsa.PrivateKey, *rsa.PublicKey, error) {
	var err error

	var signingKeyRaw []byte // aka private key
	if len(opts.SigningKeyBase64) > 0 {
		length, _ := base64.StdEncoding.Decode(signingKeyRaw, []byte(opts.SigningKeyBase64))
		signingKeyRaw = signingKeyRaw[:length]
	} else if len(opts.SigningKeyPath) > 0 {
		if signingKeyRaw, err = ioutil.ReadFile(opts.SigningKeyPath); err != nil {
			return nil, nil, fmt.Errorf("failed to open file at '%s': %s", opts.SigningKeyPath, err)
		}
	} else {
		return nil, nil, fmt.Errorf("failed to obtain a valid key for signing")
	}
	privateKeyPEM, _ := pem.Decode(signingKeyRaw)
	if privateKeyPEM.Type != "RSA PRIVATE KEY" {
		return nil, nil, fmt.Errorf("failed to find an rsa private key in the provided signing key")
	}
	privateKeyRaw := privateKeyPEM.Bytes
	if len(opts.SigningKeyPassword) > 0 {
		if privateKeyRaw, err = x509.DecryptPEMBlock(privateKeyPEM, []byte(opts.SigningKeyPassword)); err != nil {
			return nil, nil, fmt.Errorf("failed to decrypt signing key: %s", err)
		}
	}
	var parsedPrivateKey interface{}
	var pkcs1err, pkcs8err error
	parsedPrivateKey, pkcs1err = x509.ParsePKCS1PrivateKey(privateKeyRaw)
	if pkcs1err != nil {
		if parsedPrivateKey, pkcs8err = x509.ParsePKCS8PrivateKey(privateKeyRaw); pkcs8err != nil {
			return nil, nil, fmt.Errorf("failed to parse private key: %s", err)
		}
	}
	privateKey, ok := parsedPrivateKey.(*rsa.PrivateKey)
	if !ok {
		return nil, nil, fmt.Errorf("failed to typecast parsed private key into a private key")
	}

	var verifyingKeyRaw []byte // aka public key
	if len(opts.VerifyingKeyBase64) > 0 {
		length, _ := base64.StdEncoding.Decode(verifyingKeyRaw, []byte(opts.SigningKeyBase64))
		verifyingKeyRaw = verifyingKeyRaw[:length]
	} else if len(opts.VerifyingKeyPath) > 0 {
		if verifyingKeyRaw, err = ioutil.ReadFile(opts.VerifyingKeyPath); err != nil {
			return nil, nil, fmt.Errorf("failed to open file at '%s': %s", opts.VerifyingKeyPath, err)
		}
	} else {
		return nil, nil, fmt.Errorf("failed to obtain a valid key for verification of signing")
	}
	publicKeyPEM, _ := pem.Decode(verifyingKeyRaw)
	if publicKeyPEM == nil {
		return nil, nil, fmt.Errorf("failed to find an rsa public key in the provided verifying key")
	}
	publicKeyRaw := publicKeyPEM.Bytes
	parsedPublicKey, err := x509.ParsePKIXPublicKey(publicKeyRaw)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to parse public key: %s", err)
	}
	publicKey, ok := parsedPublicKey.(*rsa.PublicKey)
	if !ok {
		return nil, nil, fmt.Errorf("failed to typecast parsed public key into a public key")
	}

	return privateKey, publicKey, nil
}

// InsertSessionDataIntoRequest puts the provided session information into the provided
// request's context. The context key needs to be compatible with the function that is
// consuming the context to use the session information (default provided method is
// .GetSessionInfoFromRequest in this package)
func InsertSessionDataIntoRequest(sessionData interface{}, r *http.Request) *http.Request {
	baseContext := context.Background()
	requestContext := context.WithValue(baseContext, DefaultSessionContextKey, sessionData)
	return r.WithContext(requestContext)
}

// ParseJwtTokenOpts is the option set for the ParseJwtToken method
type ParseJwtTokenOpts struct {
	Token        string
	VerifyingKey *rsa.PublicKey
}

// ParseJwtToken is a utility function for verifying AND parsing the provided token
func ParseJwtToken(opts ParseJwtTokenOpts) (jwt.Token, error) {
	if len(opts.Token) == 0 {
		return nil, fmt.Errorf("failed to receive a token")
	}
	options := []jwt.ParseOption{}
	if opts.VerifyingKey != nil {
		publicKey, err := jwk.New(opts.VerifyingKey)
		if err != nil {
			return nil, fmt.Errorf("failed to initialise public key: %s", err)
		}
		publicKey.Set(jwk.AlgorithmKey, DefaultTokenAlgorithm)
		publicKey.Set(jwk.KeyIDKey, "verifier")
		publicKeySet := jwk.NewSet()
		publicKeySet.Add(publicKey)
		options = append(options, jwt.WithKeySet(publicKeySet), jwt.UseDefaultKey(true))
	}
	token, err := jwt.Parse([]byte(opts.Token), options...)
	if err != nil {
		return nil, fmt.Errorf("failed to parse token: %s", err)
	}
	return token, nil
}
