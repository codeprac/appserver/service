package database

import "github.com/usvc/go-config"

const (
	ConfigKeyHost     = "db-host"
	ConfigKeyPort     = "db-port"
	ConfigKeyUser     = "db-user"
	ConfigKeyPassword = "db-password"
	ConfigKeyDatabase = "db-name"
	ConfigKeySSLMode  = "db-use-ssl"
)

func GetConfiguration() config.Map {
	return config.Map{
		ConfigKeyHost: &config.String{
			Default: "localhost",
			Usage:   "hostname where the database can be found",
		},
		ConfigKeyPort: &config.Int{
			Default: 5432,
			Usage:   "port that the database is listening on",
		},
		ConfigKeyUser: &config.String{
			Default: "user",
			Usage:   "username for the user to use",
		},
		ConfigKeyPassword: &config.String{
			Default: "password",
			Usage:   "password for the user",
		},
		ConfigKeyDatabase: &config.String{
			Default: "database",
			Usage:   "name of the database",
		},
		ConfigKeySSLMode: &config.Bool{
			Usage: "defines if the sslmode should be enabled",
		},
	}
}
