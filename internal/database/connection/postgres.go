package connection

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

const (
	DefaultHost            = "localhost"
	DefaultPort     uint16 = 5432
	DefaultUser            = "username"
	DefaultPassword        = "password"
	DefaultDatabase        = "database"
	DefaultSSLMode         = false

	SSLModeEnabled  = "enable"
	SSLModeDisabled = "disable"
)

type NewPostgresConnectionOpts struct {
	Host     string  `json:"host"`
	Port     uint16  `json:"port"`
	User     string  `json:"user"`
	Password string  `json:"password"`
	Database string  `json:"database"`
	SSLMode  bool    `json:"sslMode"`
	Logger   *Logger `json:"-"`
}

func (o *NewPostgresConnectionOpts) ResolveDefaults() {
	if len(o.Host) == 0 {
		o.Host = DefaultHost
	}

	if o.Port == 0 {
		o.Port = DefaultPort
	}

	if len(o.User) == 0 {
		o.User = DefaultUser
	}

	if len(o.Password) == 0 {
		o.Password = DefaultPassword
	}

	if len(o.Database) > 0 {
		o.Database = DefaultDatabase
	}
}

func (o NewPostgresConnectionOpts) GetPasswordDefinedStatus() string {
	if len(o.Password) > 0 {
		return "yes"
	}
	return "no"
}

func (o NewPostgresConnectionOpts) GetSSLMode() string {
	if o.SSLMode {
		return SSLModeEnabled
	}
	return SSLModeDisabled
}

func NewPostgresConnection(opts NewPostgresConnectionOpts) (*gorm.DB, error) {
	opts.ResolveDefaults()

	dsn := fmt.Sprintf(
		"host=%s port=%v user=%s password=%s dbname=%s sslmode=%s",
		opts.Host,
		opts.Port,
		opts.User,
		opts.Password,
		opts.Database,
		opts.GetSSLMode(),
	)

	connectionConfig := gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "cp_",
		},
	}
	if opts.Logger != nil {
		connectionConfig.Logger = opts.Logger
	}
	connection, err := gorm.Open(postgres.Open(dsn), &connectionConfig)
	if err != nil {
		return nil, fmt.Errorf("failed to open a connection to %s@%s:%v with user %s (password: %s)", opts.Database, opts.Host, opts.Port, opts.User, opts.GetPasswordDefinedStatus())
	}
	return connection, nil
}
