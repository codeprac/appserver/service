package connection

import (
	"context"
	"time"

	"gorm.io/gorm/logger"
)

type FormattedLogger func(string, ...interface{})

type Logger struct {
	LogLevel   logger.LogLevel
	InfoLevel  FormattedLogger
	WarnLevel  FormattedLogger
	ErrorLevel FormattedLogger
}

func (l *Logger) LogMode(level logger.LogLevel) logger.Interface {
	newlogger := *l
	newlogger.LogLevel = level
	return &newlogger
}

func (l Logger) Info(_ context.Context, message string, data ...interface{}) {
	if l.LogLevel >= logger.Info {
		l.InfoLevel(message, data...)
	}
}

func (l Logger) Warn(_ context.Context, message string, data ...interface{}) {
	if l.LogLevel >= logger.Info {
		l.InfoLevel(message, data...)
	}
}

func (l Logger) Error(_ context.Context, message string, data ...interface{}) {
	if l.LogLevel >= logger.Info {
		l.InfoLevel(message, data...)
	}
}

func (l Logger) Trace(_ context.Context, _ time.Time, _ func() (string, int64), _ error) {}
