package migration

import (
	"reflect"

	"gorm.io/gorm"
)

type FormattedLog func(string, ...interface{})

func noopLog(string, ...interface{}) {}

type RunOpts struct {
	Connection  *gorm.DB
	Logger      FormattedLog
	ErrorLogger FormattedLog
	Models      []interface{}
}

func Run(opts RunOpts) []error {
	log := noopLog
	if opts.Logger != nil {
		log = opts.Logger
	}

	errorLog := noopLog
	if opts.ErrorLogger != nil {
		errorLog = opts.ErrorLogger
	}

	errors := []error{}
	for _, model := range opts.Models {
		modelInfo := reflect.TypeOf(model)
		log("starting migration of model '%s'...", modelInfo.String())
		if err := opts.Connection.AutoMigrate(model); err != nil {
			errors = append(errors, err)
			errorLog("failed to migrate model '%s': %s", modelInfo.String(), err)
			continue
		}
		log("completed migration of model '%s'", modelInfo.String())
	}
	if len(errors) > 0 {
		return errors
	}
	return nil
}
