package server

import (
	"net/http"

	"github.com/usvc/go-config"
)

const (
	ConfigKeyCORSAllowedMethods = "server-cors-allowed-methods"
	ConfigKeyCORSAllowedOrigins = "server-cors-allowed-origins"
	ConfigKeyInterface          = "server-interface"
	ConfigKeyPort               = "server-port"
)

func GetConfiguration() config.Map {
	return config.Map{
		ConfigKeyCORSAllowedMethods: &config.StringSlice{
			Default: []string{http.MethodGet, http.MethodPost, http.MethodPut},
			Usage:   "comma-delimited list of http methods to allow for the Access-Control-Allow-Methods header",
		},
		ConfigKeyCORSAllowedOrigins: &config.StringSlice{
			Default: []string{"http://localhost"},
			Usage:   "comma-delimited list of urls to allow for the Access-Control-Allow-Origin header",
		},
		ConfigKeyInterface: &config.String{
			Default: "0.0.0.0",
			Usage:   "network interface to bind to",
		},
		ConfigKeyPort: &config.Int{
			Default: 5476,
			Usage:   "network port for the server to listen on",
		},
	}
}
