package server

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/codeprac/codeprac/internal/server/middleware"
)

type NewHTTPServerOptions struct {
	Interface      string
	Port           uint16
	MaxHeaderBytes int
	AllowedMethods []string
	AllowedOrigins []string
}

func NewHTTPServer(opts NewHTTPServerOptions) *HTTPServer {
	address := fmt.Sprintf("%s:%v", opts.Interface, opts.Port)
	return &HTTPServer{
		Server: http.Server{
			Addr:              address,
			MaxHeaderBytes:    opts.MaxHeaderBytes,
			IdleTimeout:       10 * time.Second,
			ReadTimeout:       10 * time.Second,
			ReadHeaderTimeout: 10 * time.Second,
			WriteTimeout:      10 * time.Second,
		},
		CORSOptions: &HTTPCORSOptions{
			AllowedMethods:   opts.AllowedMethods,
			AllowedOrigins:   opts.AllowedOrigins,
			AllowCredentials: true,
		},
	}
}

type HttpHandler interface {
	GetHttpPath() string
	GetHttpHandlerFunc() http.HandlerFunc
}

type HTTPCORSOptions struct {
	AllowedMethods   []string
	AllowedOrigins   []string
	AllowCredentials bool
}

type HTTPServer struct {
	http.Server
	// CookieID specifies the name of the cookie that identifies session data
	CookieID string
	// CORSOptions specifies the corss-origin-resource-sharing configuration
	// for this server instance
	CORSOptions *HTTPCORSOptions
	// ErrorLogger is a log function that is used when an error occrs
	ErrorLogger func(string, ...interface{})
	// MetricsHandler defines the handler for metrics scraping
	MetricsHandler HttpHandler
	// LivenessProbeHandler defines the handler for liveness checks
	LivenessProbeHandler HttpHandler
	// ReadinessProbeHandler defines the handler for readiness checks
	ReadinessProbeHandler HttpHandler
	// RequestLogger is a log function that is used for logging requests information
	RequestLogger func(string, ...interface{})
	// TokenDataProvider returns the token data given a string-based token
	TokenDataProvider func(string) (map[string]interface{}, error)
	// TokenDataInserter adds the provided token data into the request context
	TokenDataInserter func(interface{}, *http.Request) *http.Request
}

func (s *HTTPServer) Start(handler http.Handler) error {
	mux := http.NewServeMux()
	if s.MetricsHandler != nil {
		mux.Handle(s.MetricsHandler.GetHttpPath(), s.MetricsHandler.GetHttpHandlerFunc())
	}
	if s.LivenessProbeHandler != nil {
		mux.Handle(s.LivenessProbeHandler.GetHttpPath(), s.LivenessProbeHandler.GetHttpHandlerFunc())
	}
	if s.ReadinessProbeHandler != nil {
		mux.Handle(s.ReadinessProbeHandler.GetHttpPath(), s.ReadinessProbeHandler.GetHttpHandlerFunc())
	}
	mux.Handle("/", handler)

	var primedHandler http.Handler = mux

	if s.CORSOptions != nil {
		primedHandler = middleware.AddCORS(primedHandler, middleware.AddCORSOpts{
			AllowedMethods:   s.CORSOptions.AllowedMethods,
			AllowedOrigins:   s.CORSOptions.AllowedOrigins,
			AllowCredentials: s.CORSOptions.AllowCredentials,
		})
	}

	if len(s.CookieID) > 0 && s.TokenDataProvider != nil {
		var err error
		primedHandler, err = middleware.AddSessionContext(primedHandler, middleware.AddSessionContextOpts{
			CookieID:             s.CookieID,
			ErrorLogger:          s.ErrorLogger,
			SessionTokenConsumer: s.TokenDataProvider,
			SessionTokenProvider: s.TokenDataInserter,
		})
		if err != nil {
			return fmt.Errorf("failed to attach session context middleware: %s", err)
		}
	}
	primedHandler = middleware.AddLogging(primedHandler, middleware.AddLoggingOpts{Logger: middleware.FormattedLogger(s.RequestLogger)})
	s.Server.Handler = primedHandler
	return s.Server.ListenAndServe()
}
