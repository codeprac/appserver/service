package server

const (
	DefaultLivenessProbePath  = "/healthz"
	DefaultMetricsPath        = "/metrics"
	DefaultReadinessProbePath = "/readyz"
)
