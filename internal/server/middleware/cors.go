package middleware

import (
	"net/http"

	"github.com/rs/cors"
)

type AddCORSOpts struct {
	AllowedMethods   []string
	AllowedOrigins   []string
	AllowCredentials bool
}

func AddCORS(to http.Handler, opts AddCORSOpts) http.Handler {
	return cors.New(cors.Options{
		AllowedMethods:   opts.AllowedMethods,
		AllowedOrigins:   opts.AllowedOrigins,
		AllowCredentials: opts.AllowCredentials,
	}).Handler(to)
}
