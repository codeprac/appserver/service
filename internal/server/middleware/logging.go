package middleware

import (
	"net/http"
	"time"
)

type FormattedLogger func(string, ...interface{})

type AddLoggingOpts struct {
	Logger FormattedLogger
}

type ResponseWriter struct {
	http.ResponseWriter
	ResponseSize int
	Status       int
}

func (rw ResponseWriter) Header() http.Header {
	return rw.ResponseWriter.Header()
}

func (rw *ResponseWriter) Write(output []byte) (int, error) {
	rw.ResponseSize = len(output)
	return rw.ResponseWriter.Write(output)
}

func (rw *ResponseWriter) WriteHeader(status int) {
	rw.Status = status
	rw.ResponseWriter.WriteHeader(status)
}

func AddLogging(to http.Handler, opts AddLoggingOpts) http.Handler {
	return log(to, opts)
}

func log(to http.Handler, opts AddLoggingOpts) http.HandlerFunc {
	writeLog := opts.Logger
	return func(w http.ResponseWriter, r *http.Request) {
		timeReceived := time.Now()
		wrappedResponseWriter := &ResponseWriter{ResponseWriter: w}
		to.ServeHTTP(wrappedResponseWriter, r)
		responseDuration := time.Now().Sub(timeReceived).Milliseconds()
		responseSize := wrappedResponseWriter.ResponseSize
		responseStatus := wrappedResponseWriter.Status
		writeLog("%s - %s \"%s %s\" %v %v \"%s\" \"%s\" rt=%vms",
			r.RemoteAddr,
			r.URL.User,
			r.Method,
			r.URL.Path,
			responseStatus,
			responseSize,
			r.Referer(),
			r.UserAgent(),
			responseDuration,
		)
	}
}
