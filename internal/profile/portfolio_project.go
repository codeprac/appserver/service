package profile

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/codeprac/codeprac/internal/project"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

type PortfolioProject struct {
	ID          uint             `json:"-" gorm:"primaryKey;autoIncrement"`
	UUID        string           `json:"uuid" gorm:"uniqueIndex;type:uuid;default:uuid_generate_v4()"`
	ProfileUUID string           `json:"profileUUID" gorm:"type:uuid;not null"`
	Profile     *Profile         `json:"profile,omitempty" gorm:"references:UUID"`
	ProjectUUID string           `json:"projectUUID" gorm:"type:uuid;not null"`
	Project     *project.Project `json:"project,omitempty" gorm:"references:UUID"`
	Answers     string           `json:"answers" gorm:"type:json"`
	CreatedAt   time.Time        `json:"-"`
	UpdatedAt   time.Time        `json:"-"`
	DeletedAt   time.Time        `json:"-"`
}

type CreatePortfolioProjectOpts struct {
	DatabaseConnection *gorm.DB
	PortfolioProject   *PortfolioProject
}

func (o CreatePortfolioProjectOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.PortfolioProject == nil {
		errors = append(errors, "missing portfolio project")
	} else {
		if o.PortfolioProject.ID != 0 {
			errors = append(errors, "id exists")
		}

		if o.PortfolioProject.UUID != "" {
			errors = append(errors, "uuid exists")
		}

		if o.PortfolioProject.ProfileUUID == "" {
			errors = append(errors, "missing profile uuid")
		}

		if o.PortfolioProject.ProjectUUID == "" {
			errors = append(errors, "missing project uuid")
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for creating portfolio project: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

// CreatePortfolioProject is called when the user adds a new portfolio project to their profile
func CreatePortfolioProject(opts CreatePortfolioProjectOpts) (*PortfolioProject, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to validate options: %s", err)
	}
	portfolioProjectInstance := *opts.PortfolioProject
	if query := opts.DatabaseConnection.Create(&portfolioProjectInstance); query.Error != nil {
		return nil, fmt.Errorf("failed to insert profile portfolio project into db: %s", query.Error)
	}
	return &portfolioProjectInstance, nil
}

type GetPortfoliosOpts struct {
	DatabaseConnection *gorm.DB
	ProfileUUID        string
}

// Validate ensures the provided options are valid
func (o GetPortfoliosOpts) Validate(keys ...ValidateKey) error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.ProfileUUID == "" {
		errors = append(errors, "missing profile uuid")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for getting portfolio: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

// GetPortfolios returns a list of projects that belong to the profile specified
// in the options object
//
// TODO: might be nice to optimise this
func GetPortfolios(opts GetPortfoliosOpts) ([]project.Project, error) {
	projectPortfolioInstances := []PortfolioProject{}
	search := PortfolioProject{ProfileUUID: opts.ProfileUUID}
	if query := opts.DatabaseConnection.Find(&projectPortfolioInstances, search); query.Error != nil {
		return nil, fmt.Errorf("failed to get portfolio projects for profile[%s]: %s", opts.ProfileUUID, query.Error)
	}
	projectInstances := []project.Project{}
	projectUUIDs := []string{}
	for _, projectPortfolioInstance := range projectPortfolioInstances {
		projectUUIDs = append(projectUUIDs, projectPortfolioInstance.ProjectUUID)
	}
	log.Info(opts.ProfileUUID)
	log.Info(projectPortfolioInstances)
	if query := opts.DatabaseConnection.Where("uuid IN ?", projectUUIDs).Find(&projectInstances); query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to get projects for profile[%s]: %s", opts.ProfileUUID, query.Error)
	}
	return projectInstances, nil
}
