package profile

type ValidateKey string

const (
	ValidateAccountUUID   ValidateKey = "account_uuid"
	ValidateProfileUUID   ValidateKey = "profile_uuid"
	ValidateProfileHandle ValidateKey = "profile_handle"
)
