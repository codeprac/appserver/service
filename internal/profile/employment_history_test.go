package profile

import (
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/codeprac/codeprac/internal/utils/test"
	"gorm.io/gorm"
)

type EmploymentHistoryTests struct {
	suite.Suite
	db         *sql.DB
	mock       sqlmock.Sqlmock
	connection *gorm.DB

	uuid1 string
	uuid2 string
}

func TestEmploymentHistory(t *testing.T) {
	suite.Run(t, &EmploymentHistoryTests{})
}

func (s *EmploymentHistoryTests) AfterTest(_, _ string) {
	s.db.Close()
}

func (s *EmploymentHistoryTests) BeforeTest(_, _ string) {
	s.db, s.mock, s.connection = test.GenerateGormSQLMockSetup(s)
	s.uuid1 = uuid.New().String()
	s.uuid2 = uuid.New().String()
}

func (s EmploymentHistoryTests) Test_CreateEmploymentHistory() {
	args := test.GenerateSQLMockArguments(8)
	args[0] = s.uuid1
	results := sqlmock.NewRows([]string{"uuid", "id"})

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(`INSERT INTO "employment_histories"`).WithArgs(args...).WillReturnRows(results)
	s.mock.ExpectCommit()
	_, err := CreateEmploymentHistory(CreateEmploymentHistoryOpts{
		DatabaseConnection: s.connection,
		EmploymentHistory: &EmploymentHistory{
			ProfileUUID: s.uuid1,
		},
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet())
}
