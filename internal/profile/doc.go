/*
	Package profile is an engine-level component which contains basic methods for
	performing CRUD on the various resource types associated with a user's profile.
*/
package profile
