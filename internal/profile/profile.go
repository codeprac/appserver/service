package profile

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/codeprac/codeprac/internal/account"
	"gorm.io/gorm"
)

type Profile struct {
	ID                        uint             `json:"-" gorm:"primaryKey,autoIncrement"`
	UUID                      string           `json:"uuid,omitempty" gorm:"uniqueIndex;type:uuid;default:uuid_generate_v4()"`
	AccountUUID               string           `json:"accountUUID,omitempty" gorm:"type:uuid"`
	Account                   *account.Account `json:"account,omitempty" gorm:"references:UUID"`
	Handle                    *string          `json:"handle,omitempty" gorm:"uniqueIndex"`
	AvatarURL                 *string          `json:"avatarURL,omitempty"`
	Name                      *string          `json:"name,omitempty" gorm:"type:varchar(100)"`
	PublicEmail               *string          `json:"publicEmail,omitempty" gorm:"type:varchar(256)"`
	Bio                       *string          `json:"bio,omitempty"`
	Technologies              *string          `json:"technologies,omitempty"`
	Learning                  *string          `json:"learning,omitempty"`
	Fun                       *string          `json:"fun,omitempty"`
	PasswordKey               *string          `json:"passwordKey,omitempty"`
	IsPrivate                 *bool            `json:"isPrivate,omitempty"`
	IsLookingForOpportunities *bool            `json:"isLookingForOpportunites,omitempty"`
	IsLookingForMentorship    *bool            `json:"isLookingForMentorship,omitempty"`
	IsLookingToMentor         *bool            `json:"isLookingToMentor,omitempty"`
	IsLookingToExplore        *bool            `json:"isLookingToExplore,omitempty"`
	IsLookingToHire           *bool            `json:"isLookingToHire,omitempty"`
	IsFOMO                    *bool            `json:"isFOMO,omitempty"`
	UrlBlog                   *string          `json:"urlBlog,omitempty"`
	UrlCodeMentor             *string          `json:"urlCodeMentor,omitempty"`
	UrlDev                    *string          `json:"urlDev,omitempty"`
	UrlGithub                 *string          `json:"urlGithub,omitempty"`
	UrlGitlab                 *string          `json:"urlGitlab,omitempty"`
	UrlInstagram              *string          `json:"urlInstagram,omitempty"`
	UrlLinkedIn               *string          `json:"urlLinkedIn,omitempty"`
	UrlMedium                 *string          `json:"urlMedium,omitempty"`
	UrlStackOverflow          *string          `json:"urlStackOverflow,omitempty"`
	UrlTwitter                *string          `json:"urlTwitter,omitempty"`
	UrlVimeo                  *string          `json:"urlVimeo,omitempty"`
	UrlYoutube                *string          `json:"urlYoutube,omitempty"`
	CreatedAt                 time.Time        `json:"-"`
	UpdatedAt                 time.Time        `json:"-"`
	DeletedAt                 time.Time        `json:"-"`
}

// CreateOpts provides a structure for options passed to the Create method
type CreateOpts struct {
	DatabaseConnection *gorm.DB
	AccountUUID        string
}

func (o CreateOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(o.AccountUUID) == 0 {
		errors = append(errors, "missing account uuid")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options for creating a profile: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

// Create creates a new Profile in the database for the
// account identified by the provided AccountUUID in the options. Returns a
// pointer to the Profile instance if successful, an error otherwise
func Create(opts CreateOpts) (*Profile, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to validate options: %s", err)
	}
	profileInstance := Profile{AccountUUID: opts.AccountUUID}
	query := opts.DatabaseConnection.Create(&profileInstance)
	if query.Error != nil {
		return nil, fmt.Errorf("failed to create a new profile for account with uuid '%s': %s", opts.AccountUUID, query.Error)
	}
	return &profileInstance, nil
}

// GetOpts provides a structure for the Get* methods
type GetOpts struct {
	DatabaseConnection *gorm.DB
	AccountUUID        string
	ProfileUUID        string
	ProfileHandle      string
}

// Validate ensures the provided options are valid
func (o GetOpts) Validate(keys ...ValidateKey) error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(keys) > 0 {
		for _, key := range keys {
			switch key {
			case ValidateAccountUUID:
				if len(o.AccountUUID) == 0 {
					errors = append(errors, "missing account id")
				}
			case ValidateProfileUUID:
				if len(o.ProfileUUID) == 0 {
					errors = append(errors, "missing profile id")
				}
			case ValidateProfileHandle:
				if len(o.ProfileHandle) == 0 {
					errors = append(errors, "missing profile handle")
				}
			}
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for getting a profile: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

// GetByHandle retrieves the profile identified by the provided handle
func GetByHandle(opts GetOpts) (*Profile, error) {
	if err := opts.Validate(ValidateProfileHandle); err != nil {
		return nil, fmt.Errorf("failed to get profile by handle: %s", err)
	}
	profileInstance := Profile{Handle: &opts.ProfileHandle}
	query := opts.DatabaseConnection.
		Where(profileInstance).
		First(&profileInstance)
	if query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to get profile[handle='%s'] from database: %s", opts.ProfileHandle, query.Error)
	}
	return &profileInstance, nil
}

// GetPrimary retrieves the primary profile of the provided account uuid
func GetPrimary(opts GetOpts) (*Profile, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to get profile by account uuid: %s", err)
	}
	accountInstance := account.Account{}
	if query := opts.DatabaseConnection.Where(account.Account{UUID: opts.AccountUUID}).First(&accountInstance); query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to get account with uuid '%s' from database: %s", opts.AccountUUID, query.Error)
	}
	if accountInstance.PrimaryProfileUUID == nil || len(*accountInstance.PrimaryProfileUUID) == 0 {
		return nil, nil
	}
	return Get(GetOpts{
		DatabaseConnection: opts.DatabaseConnection,
		ProfileUUID:        *accountInstance.PrimaryProfileUUID,
	})
}

func Get(opts GetOpts) (*Profile, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to get profile by uuid: %s", err)
	}
	profileInstance := Profile{UUID: opts.ProfileUUID}
	query := opts.DatabaseConnection.
		Where(profileInstance).
		First(&profileInstance)
	if query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to get profile with profile uuid '%s' from database: %s", opts.ProfileUUID, query.Error)
	}
	return &profileInstance, nil
}

// UpdateOpts provides a structure for options passed to the Update method
type UpdateOpts struct {
	DatabaseConnection *gorm.DB
	// AccountUUID together with the profile UUID let us ensure that the
	// caller (which must also be the account owner) is the identity that's
	// patching the profile
	AccountUUID string
	// UUID is the profile UUID
	UUID string
	// Patch contains only changes to the profile
	Patch Profile
}

// Update updates the Profile in the database identified by the
// provided UUID using the Patch in the options. Returns an error if the
// identified Profile could not be found or the update operation failed
func Update(opts UpdateOpts) error {
	profileInstance := Profile{UUID: opts.UUID, AccountUUID: opts.AccountUUID}
	update := opts.DatabaseConnection.Model(&profileInstance).Where(profileInstance).Updates(opts.Patch)
	if update.Error != nil {
		return fmt.Errorf("failed to update profile with uuid '%s': %s", opts.UUID, update.Error)
	}
	return nil
}
