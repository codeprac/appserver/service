package profile

// Models define the available database structures which should be created
// by the database manager
var Models = []interface{}{
	Profile{},
	EmploymentHistory{},
	PortfolioProject{},
}
