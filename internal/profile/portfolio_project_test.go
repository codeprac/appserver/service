package profile

import (
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/codeprac/codeprac/internal/utils/test"
	"gorm.io/gorm"
)

type PortfolioProjectTests struct {
	suite.Suite
	db         *sql.DB
	mock       sqlmock.Sqlmock
	connection *gorm.DB

	uuid1 string
	uuid2 string
}

func TestPortfolioProjects(t *testing.T) {
	suite.Run(t, &PortfolioProjectTests{})
}

func (s *PortfolioProjectTests) AfterTest(_, _ string) {
	s.db.Close()
}

func (s *PortfolioProjectTests) BeforeTest(_, _ string) {
	s.db, s.mock, s.connection = test.GenerateGormSQLMockSetup(s)
	s.uuid1 = uuid.New().String()
	s.uuid2 = uuid.New().String()
}
func (s PortfolioProjectTests) Test_CreatePortfolio() {
	args := test.GenerateSQLMockArguments(6)
	args[0] = s.uuid1
	args[1] = s.uuid2
	results := &sqlmock.Rows{}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(`INSERT INTO "portfolio_projects"`).WithArgs(args...).WillReturnRows(results)
	s.mock.ExpectCommit()
	_, err := CreatePortfolioProject(CreatePortfolioProjectOpts{
		DatabaseConnection: s.connection,
		PortfolioProject: &PortfolioProject{
			ProfileUUID: s.uuid1,
			ProjectUUID: s.uuid2,
		},
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet())
}

func (s PortfolioProjectTests) Test_GetPortfolios() {
	args := test.GenerateSQLMockArguments(1)
	args[0] = s.uuid1
	portfolioProjectResults := sqlmock.NewRows(
		[]string{"project_uuid"},
	)
	portfolioProjectResults.AddRow(s.uuid1)
	portfolioProjectResults.AddRow(s.uuid2)
	projectResults := sqlmock.NewRows([]string{"uuid", "id"})

	s.mock.ExpectQuery(`SELECT \* FROM \"portfolio_projects\" WHERE.+"profile_uuid"`).WithArgs(args...).WillReturnRows(portfolioProjectResults)
	s.mock.ExpectQuery(`SELECT \* FROM \"projects\" WHERE uuid IN`).WillReturnRows(projectResults)
	_, err := GetPortfolios(GetPortfoliosOpts{
		DatabaseConnection: s.connection,
		ProfileUUID:        s.uuid1,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet())
}
