package profile

import (
	"fmt"
	"strings"
	"time"

	"gorm.io/gorm"
)

type EmploymentHistory struct {
	ID               uint      `json:"-" gorm:"primaryKey,autoIncrement"`
	UUID             string    `json:"uuid" gorm:"uniqueIndex;type:uuid;default:uuid_generate_v4()"`
	ProfileUUID      string    `json:"profileUUID" gorm:"type:uuid"`
	Profile          *Profile  `json:"profile,omitempty" gorm:"references:UUID"`
	Title            string    `json:"title"`
	OrganisationName string    `json:"organisation"`
	Started          time.Time `json:"started"`
	Ended            time.Time `json:"ended"`
	CreatedAt        time.Time `json:"-"`
	UpdatedAt        time.Time `json:"-"`
	DeletedAt        time.Time `json:"-"`
}

// CreateEmploymentHistoryOpts provides a structure for options passed to the
// CreateEmploymentHistory method
type CreateEmploymentHistoryOpts struct {
	DatabaseConnection *gorm.DB
	EmploymentHistory  *EmploymentHistory
}

func (o CreateEmploymentHistoryOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.EmploymentHistory == nil {
		errors = append(errors, "missing employment history")
	} else {
		if o.EmploymentHistory.ID != 0 {
			errors = append(errors, "id already exists")
		}

		if o.EmploymentHistory.UUID != "" {
			errors = append(errors, "uuid already exists")
		}

		if o.EmploymentHistory.ProfileUUID == "" {
			errors = append(errors, "missing profile uuid")
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for creating employment history: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

// CreateEmploymentHistory creates a new EmploymentHistory in
// the database for the profile identified by the provided ProfileUUID in the options.
// Returns a pointer to the created EmploymentHistory if successfuly, an error
// otherwise
func CreateEmploymentHistory(opts CreateEmploymentHistoryOpts) (*EmploymentHistory, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to validate options: %s", err)
	}
	employmentHistoryInstance := *opts.EmploymentHistory
	create := opts.DatabaseConnection.Create(&employmentHistoryInstance)
	if create.Error != nil {
		return nil, fmt.Errorf("failed to create employment history for profile[%s]: %s", opts.EmploymentHistory.ProfileUUID, create.Error)
	}
	return &employmentHistoryInstance, nil
}
