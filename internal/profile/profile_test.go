package profile

import (
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/codeprac/codeprac/internal/utils/test"
	"gorm.io/gorm"
)

type ProfileTests struct {
	suite.Suite
	db         *sql.DB
	mock       sqlmock.Sqlmock
	connection *gorm.DB

	uuid1 string
	uuid2 string
}

func TestProfile(t *testing.T) {
	suite.Run(t, &ProfileTests{})
}

func (s *ProfileTests) AfterTest(_, _ string) {
	s.db.Close()
}

func (s *ProfileTests) BeforeTest(_, _ string) {
	s.db, s.mock, s.connection = test.GenerateGormSQLMockSetup(s)
	s.uuid1 = uuid.New().String()
	s.uuid2 = uuid.New().String()
}

func (s ProfileTests) Test_Create() {
	args := test.GenerateSQLMockArguments(32)
	args[0] = s.uuid1
	results := sqlmock.NewRows([]string{"uuid", "id"})

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(`INSERT INTO "profile`).WithArgs(args...).WillReturnRows(results)
	s.mock.ExpectCommit()
	_, err := Create(CreateOpts{
		DatabaseConnection: s.connection,
		AccountUUID:        s.uuid1,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet())
}

func (s ProfileTests) Test_Get() {
	args := test.GenerateSQLMockArguments(1)
	args[0] = s.uuid1
	results := &sqlmock.Rows{}

	s.mock.ExpectQuery(`SELECT \* FROM "accounts".+WHERE.+"uuid" =.+`).WithArgs(args...).WillReturnRows(results)
	_, err := GetPrimary(GetOpts{
		DatabaseConnection: s.connection,
		AccountUUID:        s.uuid1,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet())

	s.mock.ExpectQuery(`SELECT \* FROM "profiles".+WHERE.+"uuid" =.+`).WithArgs(args...).WillReturnRows(results)
	_, err = Get(GetOpts{
		DatabaseConnection: s.connection,
		ProfileUUID:        s.uuid1,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet())

}

func (s ProfileTests) Test_Update() {
	expectedName := "name"
	args := test.GenerateSQLMockArguments(3)
	args[0] = expectedName
	args[2] = s.uuid1
	results := sqlmock.NewResult(0, 1)

	s.mock.ExpectBegin()
	s.mock.ExpectExec(`UPDATE "profiles" SET.+"name"=.+WHERE.+"uuid" =.+`).WithArgs(args...).WillReturnResult(results)
	s.mock.ExpectCommit()
	err := Update(UpdateOpts{
		DatabaseConnection: s.connection,
		UUID:               s.uuid1,
		Patch: Profile{
			Name: &expectedName,
		},
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet())

}
