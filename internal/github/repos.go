package github

import (
	"context"
	"fmt"
	"strconv"
	"time"

	gh "github.com/google/go-github/v36/github"
	"golang.org/x/oauth2"
)

type Repo struct {
	CloneHttpURL  string          `json:"cloneHttpURL"`
	CloneSshURL   string          `json:"cloneSshURL"`
	CloneSvnURL   string          `json:"cloneSvnURL"`
	CreatedAt     time.Time       `json:"createdAt"`
	Description   string          `json:"description"`
	Forks         int             `json:"forks"`
	FullName      string          `json:"fullName"`
	HomepageURL   string          `json:"homepageURL"`
	ID            string          `json:"id"`
	IsArchived    bool            `json:"isArchived"`
	IsDisabled    bool            `json:"isDisabled"`
	IsForked      bool            `json:"isForked"`
	IsPrivate     bool            `json:"isPrivate"`
	IsTemplate    bool            `json:"isTemplate"`
	Language      string          `json:"language"`
	LicenseName   string          `json:"licenseName"`
	LicenseSPXDID string          `json:"licenseSPXDID"`
	Name          string          `json:"name"`
	OpenIssues    int             `json:"openIssues"`
	OrgUsername   string          `json:"orgUsername"`
	OwnerUsername string          `json:"ownerUsername"`
	Owner         User            `json:"owner"`
	Permissions   map[string]bool `json:"permissions"`
	Stars         int             `json:"stars"`
	Subscribers   int             `json:"subscribers"`
	UpdatedAt     time.Time       `json:"updatedAt"`
	URL           string          `json:"url"`
	Visibility    string          `json:"visibility"`
	Watchers      int             `json:"watchers"`
}

func GetAuthenticatedUserRepos(accessToken string, page int, perPage int) ([]Repo, error) {
	ctx := context.Background()
	client := gh.NewClient(oauth2.NewClient(ctx, oauth2.StaticTokenSource(&oauth2.Token{AccessToken: accessToken})))
	repos, _, err := client.Repositories.List(ctx, "", &gh.RepositoryListOptions{
		Direction: "desc",
		ListOptions: gh.ListOptions{
			Page:    page,
			PerPage: perPage,
		},
		Sort: "updated",
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get user repos: %s", err)
	}
	repoList := []Repo{}
	for _, repo := range repos {
		repoList = append(repoList, serializeGithubRepo(repo))
	}
	return repoList, nil
}

func serializeGithubRepo(repo *gh.Repository) Repo {
	return Repo{
		CloneHttpURL:  repo.GetGitURL(),
		CloneSshURL:   repo.GetSSHURL(),
		CloneSvnURL:   repo.GetSVNURL(),
		CreatedAt:     repo.GetCreatedAt().Time,
		Description:   repo.GetDescription(),
		Forks:         repo.GetForksCount(),
		FullName:      repo.GetFullName(),
		HomepageURL:   repo.GetHomepage(),
		ID:            strconv.FormatInt(repo.GetID(), 10),
		IsArchived:    repo.GetArchived(),
		IsDisabled:    repo.GetDisabled(),
		IsForked:      repo.GetFork(),
		IsPrivate:     repo.GetPrivate(),
		IsTemplate:    repo.GetIsTemplate(),
		Language:      repo.GetLanguage(),
		LicenseName:   repo.GetLicense().GetName(),
		LicenseSPXDID: repo.GetLicense().GetSPDXID(),
		Name:          repo.GetName(),
		OpenIssues:    repo.GetOpenIssuesCount(),
		OrgUsername:   repo.GetOrganization().GetLogin(),
		OwnerUsername: repo.GetOwner().GetLogin(),
		Owner:         serializeGithubUser(repo.GetOwner()),
		Permissions:   repo.GetPermissions(),
		Stars:         repo.GetStargazersCount(),
		Subscribers:   repo.GetSubscribersCount(),
		UpdatedAt:     repo.GetUpdatedAt().Time,
		URL:           repo.GetHTMLURL(),
		Visibility:    repo.GetVisibility(),
		Watchers:      repo.GetWatchersCount(),
	}
}
