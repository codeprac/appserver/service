package github

import "github.com/usvc/go-config"

const (
	ConfigKeyClientID     = "github-client-id"
	ConfigKeyClientSecret = "github-client-secret"
	ConfigKeyRedirectURI  = "github-redirect-uri"
)

func GetConfiguration() config.Map {
	return config.Map{
		ConfigKeyClientID: &config.String{
			Usage: "Github client ID",
		},
		ConfigKeyClientSecret: &config.String{
			Usage: "Github client secret",
		},
		ConfigKeyRedirectURI: &config.String{
			Usage: "Github redirect uri",
		},
	}
}
