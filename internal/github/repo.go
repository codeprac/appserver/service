package github

import (
	"context"
	"fmt"
	"net/http"

	gh "github.com/google/go-github/v36/github"
	"golang.org/x/oauth2"
)

type RepoInfo struct {
	Repo
	Languages    map[string]int `json:"languages"`
	Contributors []User         `json:"contributors"`
}

func GetRepositoryInfo(accessToken, owner, repoName string) (*RepoInfo, error) {
	ctx := context.Background()
	client := gh.NewClient(oauth2.NewClient(ctx, oauth2.StaticTokenSource(&oauth2.Token{AccessToken: accessToken})))
	repo, _, err := client.Repositories.Get(ctx, owner, repoName)
	if err != nil {
		return nil, fmt.Errorf("failed to get repository '%s/%s': %s", owner, repoName, err)
	}
	repoInfo := &RepoInfo{
		Repo: serializeGithubRepo(repo),
	}

	// languages
	req, err := client.NewRequest(http.MethodGet, repo.GetLanguagesURL(), nil)
	if err != nil {
		return repoInfo, fmt.Errorf("failed to build request to get languages: %s", err)
	}
	_, err = client.Do(context.Background(), req, &repoInfo.Languages)
	if err != nil {
		return repoInfo, fmt.Errorf("failed to get languages: %s", err)
	}

	// contributors
	contributors := []gh.User{}
	req, err = client.NewRequest(http.MethodGet, repo.GetContributorsURL(), nil)
	if err != nil {
		return repoInfo, fmt.Errorf("failed to build request to get contributors: %s", err)
	}
	_, err = client.Do(context.Background(), req, &contributors)
	if err != nil {
		return repoInfo, fmt.Errorf("failed to get languages: %s", err)
	}
	for _, contributor := range contributors {
		repoInfo.Contributors = append(repoInfo.Contributors, serializeGithubUser(&contributor))
	}

	return repoInfo, nil
}
