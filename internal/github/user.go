package github

import (
	"context"
	"fmt"
	"strconv"

	gh "github.com/google/go-github/v36/github"
	"golang.org/x/oauth2"
)

// User is the representation of a github.User instance for internal use
// in Codeprac
type User struct {
	AvatarURL                string `json:"avatarURL"`
	Bio                      string `json:"bio,omitempty"`
	Blog                     string `json:"blog,omitempty"`
	Company                  string `json:"company,omitempty"`
	Email                    string `json:"email,omitempty"`
	Followers                int    `json:"followers"`
	Following                int    `json:"following"`
	Hireable                 bool   `json:"hireable"`
	ID                       string `json:"id,omitempty"`
	Location                 string `json:"location,omitempty"`
	Name                     string `json:"name,omitempty"`
	PrivateGistsCount        int    `json:"privateGistsCount"`
	PrivateRepositoriesCount int    `json:"privateRepositoriesCount"`
	PublicGistsCount         int    `json:"publicGistsCount"`
	PublicRepositoriesCount  int    `json:"publicRepositoriesCount"`
	Username                 string `json:"username"`
}

// GetAuthenticatedUser is an engine function that retrieves the User object
// for the user represented by the provided access token
func GetAuthenticatedUser(accessToken string) (*User, error) {
	ctx := context.Background()
	client := gh.NewClient(oauth2.NewClient(ctx, oauth2.StaticTokenSource(&oauth2.Token{AccessToken: accessToken})))
	githubUser, _, err := client.Users.Get(ctx, "")
	if err != nil {
		return nil, fmt.Errorf("failed to get user information: %s", err)
	}
	emailAddresses, _, err := client.Users.ListEmails(ctx, &gh.ListOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to get user emails: %s", err)
	}
	chosenEmailAddress := emailAddresses[0].GetEmail()
	for _, emailAddress := range emailAddresses {
		if emailAddress.GetPrimary() {
			chosenEmailAddress = emailAddress.GetEmail()
			break
		}
	}
	githubUser.Email = &chosenEmailAddress
	user := serializeGithubUser(githubUser)
	return &user, nil
}

func serializeGithubUser(githubUser *gh.User) User {
	return User{
		AvatarURL:                githubUser.GetAvatarURL(),
		Bio:                      githubUser.GetBio(),
		Blog:                     githubUser.GetBlog(),
		Company:                  githubUser.GetCompany(),
		Email:                    githubUser.GetEmail(),
		Followers:                githubUser.GetFollowers(),
		Following:                githubUser.GetFollowing(),
		Hireable:                 githubUser.GetHireable(),
		ID:                       strconv.FormatInt(githubUser.GetID(), 10),
		Location:                 githubUser.GetLocation(),
		Name:                     githubUser.GetName(),
		PrivateGistsCount:        githubUser.GetPrivateGists(),
		PrivateRepositoriesCount: githubUser.GetOwnedPrivateRepos(),
		PublicGistsCount:         githubUser.GetPublicGists(),
		PublicRepositoriesCount:  githubUser.GetPublicRepos(),
		Username:                 githubUser.GetLogin(),
	}
}
