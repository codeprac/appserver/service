package client

import "github.com/usvc/go-config"

const (
	ConfigKeyClientBaseURL     = "client-base-url"
	ConfigKeyClientGatewayPath = "client-gateway-path"
)

func GetConfiguration() config.Map {
	return config.Map{
		ConfigKeyClientBaseURL: &config.String{
			Default: "http://localhost:3000",
			Usage:   "defines the base url which redirections to the client web application will be constructed from",
		},
		ConfigKeyClientGatewayPath: &config.String{
			Default: "/gateway",
			Usage:   "defines the path of the client service to route to after successful authentication",
		},
	}
}
