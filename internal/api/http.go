package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/codeprac/codeprac/internal/api/account"
	"gitlab.com/codeprac/codeprac/internal/api/auth"
	"gitlab.com/codeprac/codeprac/internal/api/profile"
	"gitlab.com/codeprac/codeprac/internal/api/project"
	"gitlab.com/codeprac/codeprac/internal/api/session"
	"gitlab.com/codeprac/codeprac/internal/api/unauth"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/constants"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func GetHttpHandler(opts APIOpts) (http.Handler, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to validate provided configuration for the api: %s", err)
	}
	respondWith = utils.HttpResponder{
		Source:      "/",
		ErrorLogger: utils.LogAs(log.Warnf),
	}

	router := mux.NewRouter()

	if err := account.RouteWith(router, account.RouteOpts{
		DatabaseConnection: opts.DatabaseConnection,
	}); err != nil {
		return nil, fmt.Errorf("failed to add routes for /account/**: %s", err)
	}

	if err := auth.RouteWith(router, auth.RouteOpts{
		ClientBaseURL:      opts.ClientBaseURL,
		DatabaseConnection: opts.DatabaseConnection,
		GithubClientID:     opts.GithubClientID,
		GithubClientSecret: opts.GithubClientSecret,
		GithubRedirectURI:  opts.GithubRedirectURI,
	}); err != nil {
		return nil, fmt.Errorf("failed to add routes for /auth/**: %s", err)
	}

	if err := profile.RouteWith(router, profile.RouteOpts{
		DatabaseConnection: opts.DatabaseConnection,
	}); err != nil {
		return nil, fmt.Errorf("failed to add routes for /profile/**: %s", err)
	}

	if err := project.RouteWith(router, project.RouteOpts{
		DatabaseConnection: opts.DatabaseConnection,
	}); err != nil {
		return nil, fmt.Errorf("failed to add routes for /project[s]/**: %s", err)
	}

	if err := session.RouteWith(router, session.RouteOpts{
		ClientBaseURL:      opts.ClientBaseURL,
		DatabaseConnection: opts.DatabaseConnection,
		SigningKey:         &opts.SessionSigningKey,
		VerifyingKey:       &opts.SessionVerifyingKey,
	}); err != nil {
		return nil, fmt.Errorf("failed to add routes for /session/**: %s", err)
	}

	if err := unauth.RouteWith(router, unauth.RouteOpts{
		ClientBaseURL:      opts.ClientBaseURL,
		DatabaseConnection: opts.DatabaseConnection,
	}); err != nil {
		return nil, fmt.Errorf("failed to add routes for /unauth/**: %s", err)
	}

	router.
		Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			respondWith.OK(map[string]interface{}{
				"version": constants.Version,
			}).Send(w)
		})).
		Methods("GET")

	return router, nil
}
