package github

const (
	FieldClientId    = "client_id"
	FieldRedirectUri = "redirect_uri"
	FieldScopes      = "scope"
	FieldState       = "state"
)

var loginScopes = []string{"user"}
