package github

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/types"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

var respondWith utils.HttpResponder

type RouteOpts struct {
	ClientBaseURL      string
	ClientID           string
	ClientSecret       string
	DatabaseConnection *gorm.DB
	RedirectURI        string
}

func (o RouteOpts) Validate() error {
	errors := []string{}

	if o.ClientBaseURL == "" {
		errors = append(errors, "missing client base url")
	}

	if o.ClientID == "" {
		errors = append(errors, "missing client id")
	}

	if o.ClientSecret == "" {
		errors = append(errors, "missing client secret")
	}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.RedirectURI == "" {
		errors = append(errors, "missing redirect uri")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate route opts for /auth/github: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func RouteWith(router types.Router, opts RouteOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to validate route options: %s", err)
	}
	respondWith = utils.HttpResponder{
		Source:      "auth/github",
		ErrorLogger: utils.LogAs(log.Warnf),
	}
	githubRouter := router.PathPrefix("/github").Subrouter()
	githubRouter.Handle("", get(opts)).Methods(http.MethodGet)
	return nil
}
