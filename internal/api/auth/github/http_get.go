package github

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/github"
	"gitlab.com/codeprac/codeprac/internal/session"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"golang.org/x/oauth2"
	oauth2github "golang.org/x/oauth2/github"
	"gorm.io/gorm"
)

func get(opts RouteOpts) http.HandlerFunc {
	oauthConfig := oauth2.Config{
		ClientID:     opts.ClientID,
		ClientSecret: opts.ClientSecret,
		RedirectURL:  opts.RedirectURI,
		Scopes:       loginScopes,
		Endpoint:     oauth2github.Endpoint,
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				log.Warn(r)
			}
		}()
		var err error
		requestQuery := r.URL.Query()

		authCode := requestQuery.Get("code")
		authState := requestQuery.Get("state")

		isCodePresent := len(authCode) > 0
		isStatePresent := len(authState) > 0

		if isCodePresent && isStatePresent {
			log.Infof("handling successful oauth flow with state string '%s'...", authState)
			var token *oauth2.Token
			var githubUserInstance *github.User
			var accountInstance *account.Account
			var sessionInstance *session.Session
			var isNewUser = func(a *account.Account) bool { return a == nil }

			if token, err = handleTokenExchange(oauthConfig, authCode, w); err != nil {
				panic(err)
			}
			log.Infof("successfully exchanged code '%s' for token state string '%s'", authCode, authState)

			if githubUserInstance, err = handleGithubUserRetrieval(token.AccessToken, w); err != nil {
				panic(err)
			}
			log.Infof("successfully retrieved github user from state string '%s' (username: '%s', id: '%s')", authState, githubUserInstance.Username, githubUserInstance.ID)

			if accountInstance, err = handleAccountRetrieval(token.AccessToken, githubUserInstance, opts.DatabaseConnection, w); err != nil {
				panic(err)
			}
			if isNewUser(accountInstance) {
				log.Infof("creating account for github user with state string '%s'", authState)
				if accountInstance, err = handleAccountCreation(token.AccessToken, githubUserInstance, opts.DatabaseConnection, w); err != nil {
					panic(err)
				}
			}
			if sessionInstance, err = handleSessionCreation(opts.DatabaseConnection, *accountInstance, w); err != nil {
				panic(err)
			}
			respondWith.OK(sessionInstance.Token).Send(w)
			return
		}
		if isStatePresent {
			if err := handleRedirectToAuthenticationPage(oauthConfig, authState, w); err != nil {
				panic(err)
			}
		}
		respondWith.BadRequest("auth_github_invalid_request").Send(w)
	})
}

// handleAccountCreation does the creation of an account and its linked account using
// details from the provided github user
func handleAccountCreation(
	accessToken string,
	githubUser *github.User,
	databaseConnection *gorm.DB,
	w http.ResponseWriter,
) (*account.Account, error) {
	platformData, err := json.Marshal(githubUser)
	if err != nil {
		respondWith.InternalServerError("auth_github_error_json").Send(w)
		return nil, fmt.Errorf("failed to convert github user with id %v and username '%s' into a json string: %s", githubUser.ID, githubUser.Username, err)
	}
	accountInstance, err := account.Create(account.CreateOpts{
		DatabaseConnection: databaseConnection,
		Account:            account.Account{},
	})
	if err != nil {
		respondWith.InternalServerError("auth_github_create_account_failed").Send(w)
		return nil, fmt.Errorf("failed to create an account for github user with id %v and username '%s': %s", githubUser.ID, githubUser.Username, err)
	}
	_, err = account.CreateLinkedAccount(account.CreateLinkedAccountOpts{
		DatabaseConnection: databaseConnection,
		LinkedAccount: account.LinkedAccount{
			AccountID:      accountInstance.ID,
			PlatformID:     github.PlatformID,
			PlatformUserID: githubUser.ID,
			PlatformData:   string(platformData),
			AccessToken:    accessToken,
		},
	})
	if err != nil {
		respondWith.InternalServerError("auth_github_create_linked_account_failed").Send(w)
		return nil, fmt.Errorf("failed to create a linked account for github user with id %v and username '%s': %s", githubUser.ID, githubUser.Username, err)
	}
	return accountInstance, nil
}

// handleAccountRetrieval retrieves a Codeprac account given the user's github
// uesr; if no such user was found, both return arguments will be nil.
//
// Also, there could be a situation where the linked account exists but not the
// account if the user drops off during logical account creation. In such cases,
// just recreate the linked account via upserting :D
func handleAccountRetrieval(accessToken string, githubUser *github.User, databaseConnection *gorm.DB, w http.ResponseWriter) (*account.Account, error) {
	linkedAccountInstance, err := account.GetLinkedAccount(account.GetLinkedAccountOpts{
		DatabaseConnection: databaseConnection,
		PlatformID:         github.PlatformID,
		PlatformUserID:     githubUser.ID,
	})
	if err != nil {
		respondWith.InternalServerError("auth_github_get_linked_account_failed").Send(w)
		return nil, fmt.Errorf("failed to retrieve a linked account for github user with id %v and username '%s': %s", githubUser.ID, githubUser.Username, err)
	}
	if linkedAccountInstance == nil {
		return nil, nil
	}
	// update linekd account with new token
	if err := account.UpdateLinkedAccount(account.UpdateLinkedAccountOpts{
		DatabaseConnection: databaseConnection,
		UUID:               linkedAccountInstance.UUID,
		Patch: account.LinkedAccount{
			AccessToken: accessToken,
		},
	}); err != nil {
		respondWith.InternalServerError("auth_github_update_linked_account_failed").Send(w)
		return nil, fmt.Errorf("failed to update linked account with new access token: %s", err)
	}
	accountInstance, err := account.GetByID(account.GetOpts{DatabaseConnection: databaseConnection, ID: linkedAccountInstance.AccountID})
	if err != nil {
		respondWith.InternalServerError("auth_github_get_account_failed").Send(w)
		return nil, fmt.Errorf("failed to retrieve an account given github user with id %v and username '%s': %s", githubUser.ID, githubUser.Username, err)
	}
	if accountInstance == nil {
		return nil, nil
	}
	accountInstance.LinkedAccounts = []account.LinkedAccount{*linkedAccountInstance}
	return accountInstance, nil
}

// handleGithubUserRetrieval retrieves the github user
func handleGithubUserRetrieval(accessToken string, w http.ResponseWriter) (*github.User, error) {
	githubUserInstance, err := github.GetAuthenticatedUser(accessToken)
	if err != nil {
		respondWith.InternalServerError("auth_github_get_github_user_failed").Send(w)
		return nil, fmt.Errorf("failed to retrieve authenticated github user: %s", err)
	}
	return githubUserInstance, nil
}

// handleRedirectToAuthenticationPage is an engine component that handles the http-based
// redirection to the github oauth2 endpoint
func handleRedirectToAuthenticationPage(oauthConfig oauth2.Config, state string, w http.ResponseWriter) error {
	redirectToURL, err := url.Parse(oauth2github.Endpoint.AuthURL)
	if err != nil {
		respondWith.InternalServerError("auth_github_bad_auth_url", oauth2github.Endpoint.AuthURL).Send(w)
		return fmt.Errorf("failed to parse oauth2 authentication url to redirect to: %s", err)
	}
	vals := redirectToURL.Query()
	vals.Add(FieldClientId, oauthConfig.ClientID)
	vals.Add(FieldRedirectUri, oauthConfig.RedirectURL)
	vals.Add(FieldScopes, strings.Join(oauthConfig.Scopes, " "))
	vals.Add(FieldState, state)
	redirectToURL.RawQuery = vals.Encode()
	respondWith.TemporaryRedirect(redirectToURL.String()).Send(w)
	return nil
}

// handleSessionCreation returns a session token or else responds with an appropriate error
func handleSessionCreation(databaseConnection *gorm.DB, accountInstance account.Account, w http.ResponseWriter) (*session.Session, error) {
	opts := session.CreateSessionOpts{
		DatabaseConnection: databaseConnection,
		AccountUUID:        accountInstance.UUID,
	}
	sessionToken, err := session.CreateSession(opts)
	if err != nil {
		respondWith.InternalServerError("auth_github_create_session_failed").Send(w)
		return nil, fmt.Errorf("failed to create a session for account with id %v: %s", accountInstance.ID, err)
	}
	return sessionToken, nil
}

// handleTokenExchange abstracts away the token exchange and error-handling logic
// from the main function
func handleTokenExchange(oauthConfig oauth2.Config, authenticationCode string, w http.ResponseWriter) (*oauth2.Token, error) {
	ctx := context.Background()
	token, err := oauthConfig.Exchange(ctx, authenticationCode)
	if err != nil {
		respondWith.BadRequest("auth_github_token_exchange_failed").Send(w)
		return nil, fmt.Errorf("failed to exchange authentication code for an access token: %s", err)
	}
	return token, nil
}
