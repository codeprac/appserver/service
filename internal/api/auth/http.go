package auth

import (
	"fmt"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/auth/github"
	"gitlab.com/codeprac/codeprac/internal/api/types"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

var respondWith utils.HttpResponder

type RouteOpts struct {
	ClientBaseURL      string
	DatabaseConnection *gorm.DB
	GithubClientID     string
	GithubClientSecret string
	GithubRedirectURI  string
}

func (o RouteOpts) Validate() error {
	errors := []string{}

	if o.ClientBaseURL == "" {
		errors = append(errors, "missing client base url")
	}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.GithubClientID == "" {
		errors = append(errors, "missing github client id")
	}

	if o.GithubClientSecret == "" {
		errors = append(errors, "missing github client secret")
	}

	if o.GithubRedirectURI == "" {
		errors = append(errors, "missing github redirect uri")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options for /auth/**: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func RouteWith(router types.Router, opts RouteOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to route /auth/**: %s", err)
	}

	respondWith = utils.HttpResponder{
		Source:      "auth",
		ErrorLogger: utils.LogAs(log.Warnf),
	}

	authRouter := router.PathPrefix("/auth").Subrouter()

	if err := github.RouteWith(authRouter, github.RouteOpts{
		ClientBaseURL:      opts.ClientBaseURL,
		ClientID:           opts.GithubClientID,
		ClientSecret:       opts.GithubClientSecret,
		DatabaseConnection: opts.DatabaseConnection,
		RedirectURI:        opts.GithubRedirectURI,
	}); err != nil {
		return fmt.Errorf("failed to route /auth/github/**: %s", err)
	}

	return nil
}
