package profile

// PutProfileRequestSerializer provides a structure for serializing the incoming request
// from a client. These should align with the structures defined on the client-side.
// Maybe implement some contract test for this in future if things start going
// messy often
type PutProfileRequestSerializer struct {
	Handle                    *string `json:"handle,omitempty"`
	AvatarURL                 *string `json:"avatarURL,omitempty"`
	Name                      *string `json:"name,omitempty"`
	PublicEmail               *string `json:"publicEmail,omitempty"`
	Bio                       *string `json:"bio,omitempty"`
	Technologies              *string `json:"technologies,omitempty"`
	Learning                  *string `json:"learning,omitempty"`
	Fun                       *string `json:"fun,omitempty"`
	PasswordKey               *string `json:"passwordKey,omitempty"`
	IsPrivate                 *bool   `json:"isPrivate,omitempty"`
	IsLookingForOpportunities *bool   `json:"isLookingForOpportunites,omitempty"`
	IsLookingForMentorship    *bool   `json:"isLookingForMentorship,omitempty"`
	IsLookingToMentor         *bool   `json:"isLookingToMentor,omitempty"`
	IsLookingToExplore        *bool   `json:"isLookingToExplore,omitempty"`
	IsLookingToHire           *bool   `json:"isLookingToHire,omitempty"`
	IsFOMO                    *bool   `json:"isFOMO,omitempty"`
	UrlBlog                   *string `json:"urlBlog,omitempty"`
	UrlCodeMentor             *string `json:"urlCodeMentor,omitempty"`
	UrlDev                    *string `json:"urlDev,omitempty"`
	UrlGithub                 *string `json:"urlGithub,omitempty"`
	UrlGitlab                 *string `json:"urlGitlab,omitempty"`
	UrlInstagram              *string `json:"urlInstagram,omitempty"`
	UrlLinkedIn               *string `json:"urlLinkedIn,omitempty"`
	UrlMedium                 *string `json:"urlMedium,omitempty"`
	UrlStackOverflow          *string `json:"urlStackOverflow,omitempty"`
	UrlTwitter                *string `json:"urlTwitter,omitempty"`
	UrlVimeo                  *string `json:"urlVimeo,omitempty"`
	UrlYoutube                *string `json:"urlYoutube,omitempty"`
}
