package profile

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/profile"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func put(opts RouteOpts) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		vars := mux.Vars(r)
		profileUUID := vars["uuid"]
		if _, err := uuid.Parse(profileUUID); err != nil {
			respondWith.BadRequest("profile_invalid_uuid").Send(w)
			return
		}

		requestBody, err := io.ReadAll(r.Body)
		if err != nil {
			respondWith.BadRequest("profile_invalid_body").Send(w)
			return
		}
		var serializedStructure PutProfileRequestSerializer
		if err := json.Unmarshal(requestBody, &serializedStructure); err != nil {
			log.Warnf("failed to unmarshal data '%s': %s", string(requestBody), err)
			respondWith.BadRequest("profile_invalid_data").Send(w)
			return
		}
		// error is ignored here since we just unmarshalled it and if that was
		// successful, there's no reason this will not be, let the fella panicK
		// if it reaches such a point
		serializedData, _ := json.Marshal(serializedStructure)
		var profilePatch profile.Profile
		if err := json.Unmarshal(serializedData, &profilePatch); err != nil {
			respondWith.InternalServerError("profile_incompatible_serializer").Send(w)
			return
		}

		if err := profile.Update(profile.UpdateOpts{
			DatabaseConnection: opts.DatabaseConnection,
			AccountUUID:        authInfo.GetAccountUUID(),
			UUID:               profileUUID,
			Patch:              profilePatch,
		}); err != nil {
			log.Warnf("failed to update profile with uuid '%s' in database: %s", profileUUID, err)
			respondWith.InternalServerError("profile_update_database_error").Send(w)
			return
		}

		respondWith.OK(profilePatch).Send(w)
	})
}
