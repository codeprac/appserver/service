package profile

import (
	"net/http"

	"gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/profile"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func post(opts RouteOpts) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		profileInstance, err := profile.Create(profile.CreateOpts{
			DatabaseConnection: opts.DatabaseConnection,
			AccountUUID:        authInfo.GetAccountUUID(),
		})
		if err != nil {
			log.Warnf("failed to create profile for account with uuid '%s': %s", authInfo.GetAccountUUID(), err)
			respondWith.InternalServerError("profile_create_failed").Send(w)
			return
		}
		if profileInstance == nil {
			respondWith.InternalServerError("profile_empty").Send(w)
			return
		}

		if err := account.Update(account.UpdateOpts{
			DatabaseConnection: opts.DatabaseConnection,
			UUID:               authInfo.GetAccountUUID(),
			Patch: account.Account{
				PrimaryProfileUUID: &profileInstance.UUID,
			},
		}); err != nil {
			respondWith.InternalServerError("profile_not_linked_to_account").Send(w)
			return
		}

		respondWith.OK(profileInstance).Send(w)
	})
}
