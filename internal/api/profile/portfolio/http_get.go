package portfolio

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/profile"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func get(opts RouteOpts) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, authError := utils.GetAuthInfo(w, r)
		vars := mux.Vars(r)
		profileUUID := vars["uuid"]
		isProfileUuidInPath := profileUUID != ""
		if !isProfileUuidInPath {
			if authError != nil {
				log.Warn(authError)
				return
			}
			accountInstance, err := account.GetByUUID(account.GetOpts{
				DatabaseConnection: opts.DatabaseConnection,
				UUID:               authInfo.GetAccountUUID(),
			})
			if err != nil {
				respondWith.NotFound("profile_portfolio_account_not_found").Send(w)
				return
			}
			profileUUID = *accountInstance.PrimaryProfileUUID
		}
		if profileUUID == "" {
			respondWith.NotFound("profile_portfolio_profile_not_found").Send(w)
			return
		}

		projects, err := profile.GetPortfolios(profile.GetPortfoliosOpts{
			DatabaseConnection: opts.DatabaseConnection,
			ProfileUUID:        profileUUID,
		})
		if err != nil {
			respondWith.InternalServerError("profile_portfolio_db_list").Send(w)
			return
		}

		respondWith.OK(projects).Send(w)
	})
}
