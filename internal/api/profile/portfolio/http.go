package portfolio

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/types"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

var respondWith utils.HttpResponder

type RouteOpts struct {
	DatabaseConnection *gorm.DB
}

func (o RouteOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate route options for /portfolio: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func RouteWith(router types.Router, opts RouteOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to route /profile/portfolio/** endpoints: %s", err)
	}
	respondWith = utils.HttpResponder{
		Source:      "profile/portfolio",
		ErrorLogger: utils.LogAs(log.Warnf),
	}

	portfolioRouter := router.PathPrefix("/portfolio").Subrouter()
	portfoliosRouter := router.PathPrefix("/portfolios").Subrouter()
	portfoliosRouter.Handle("", get(opts)).Methods(http.MethodGet)
	router.Handle("/{uuid}/portfolios", get(opts)).Methods(http.MethodGet)
	portfolioRouter.Handle("", post(opts)).Methods(http.MethodPost)
	router.Handle("/{uuid}/portfolio", post(opts)).Methods(http.MethodPost)

	return nil
}
