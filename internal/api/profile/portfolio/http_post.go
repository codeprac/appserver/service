package portfolio

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/profile"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

type PostPortfolioRequest struct {
	UUID              string `json:"uuid"`
	PlatformID        string `json:"platformID"`
	PlatformProjectID string `json:"platformProjectID"`
	Answers           string `json:"answers"`
}

func post(opts RouteOpts) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		vars := mux.Vars(r)
		profileUUID := vars["uuid"]
		isProfileUuidInPath := profileUUID != ""
		if !isProfileUuidInPath {
			accountInstance, err := account.GetByUUID(account.GetOpts{
				DatabaseConnection: opts.DatabaseConnection,
				UUID:               authInfo.GetAccountUUID(),
			})
			if err != nil {
				respondWith.NotFound("profile_portfolio_account_not_found").Send(w)
				return
			}
			profileUUID = *accountInstance.PrimaryProfileUUID
		}
		if profileUUID == "" {
			respondWith.NotFound("profile_portfolio_profile_not_found").Send(w)
			return
		}

		var requestBody PostPortfolioRequest
		body, err := io.ReadAll(r.Body)
		if err != nil {
			log.Warnf("failed to read request body: %s", err)
			respondWith.BadRequest("profile_portfolio_invalid_body").Send(w)
			return
		} else if err = json.Unmarshal(body, &requestBody); err != nil {
			log.Warnf("failed to unmarshal '%s': %s", string(body), err)
			respondWith.BadRequest("profile_portfolio_invalid_body").Send(w)
			return
		}

		projectUUID := requestBody.UUID
		answers := `""`
		if requestBody.Answers != "" {
			answers = requestBody.Answers
		}

		log.Infof("setting project[%s] as portfolio project for profile[%s]", projectUUID, profileUUID)
		portfolioProjectInstance := profile.PortfolioProject{
			ProfileUUID: profileUUID,
			ProjectUUID: projectUUID,
			Answers:     answers,
		}
		portfolioProject, err := profile.CreatePortfolioProject(profile.CreatePortfolioProjectOpts{
			DatabaseConnection: opts.DatabaseConnection,
			PortfolioProject:   &portfolioProjectInstance,
		})
		if err != nil {
			log.Warnf("failed to create portfolio project in the db: %s", err)
			respondWith.InternalServerError("profile_portfolio_db_save").Send(w)
			return
		}

		respondWith.OK(portfolioProject).Send(w)
	})
}
