package profile

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/profile"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func get(opts RouteOpts) http.HandlerFunc {
	get := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		profileHandle := vars["handle"]
		if profileHandle != "" {
			profileInstance, err := profile.GetByHandle(profile.GetOpts{
				DatabaseConnection: opts.DatabaseConnection,
				ProfileHandle:      profileHandle,
			})
			if err != nil {
				respondWith.InternalServerError("profile_database_error").Send(w)
				return
			}
			if profileInstance == nil {
				respondWith.NotFound("profile_not_found").Send(w)
				return
			}

			respondWith.OK(profileInstance).Send(w)
			return
		}

		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}
		profileInstance, err := profile.GetPrimary(profile.GetOpts{
			DatabaseConnection: opts.DatabaseConnection,
			AccountUUID:        authInfo.GetAccountUUID(),
		})
		if err != nil {
			respondWith.InternalServerError("profile_database_error").Send(w)
			return
		}
		if profileInstance == nil {
			respondWith.NotFound("profile_not_found").Send(w)
			return
		}

		respondWith.OK(profileInstance).Send(w)
	})
	return get
}
