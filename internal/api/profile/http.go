package profile

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/profile/portfolio"
	"gitlab.com/codeprac/codeprac/internal/api/types"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

var respondWith utils.HttpResponder

type RouteOpts struct {
	DatabaseConnection *gorm.DB
}

func (o RouteOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate route options for /profile: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func RouteWith(router types.Router, opts RouteOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to route profile endpoints: %s", err)
	}
	respondWith = utils.HttpResponder{
		Source:      "profile",
		ErrorLogger: utils.LogAs(log.Warnf),
	}
	profileRouter := router.PathPrefix("/profile").Subrouter()
	if err := portfolio.RouteWith(profileRouter, portfolio.RouteOpts{
		DatabaseConnection: opts.DatabaseConnection,
	}); err != nil {
		return fmt.Errorf("failed to route /profile/portfolio/** endpoints: %s", err)
	}
	profileRouter.Handle("", get(opts)).Methods(http.MethodGet)
	profileRouter.Handle("/{handle}", get(opts)).Methods(http.MethodGet)
	profileRouter.Handle("", post(opts)).Methods(http.MethodPost)
	profileRouter.Handle("/{uuid}", put(opts)).Methods(http.MethodPut)

	return nil
}
