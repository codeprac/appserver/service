package session

import (
	"net/http"

	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func get(opts RouteOpts) http.HandlerFunc {
	get := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		respondWith.OK(authInfo.GetAccountUUID()).Send(w)
	})

	return get
}
