package session

import (
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/session"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gitlab.com/codeprac/codeprac/pkg/routes"
)

func getToken(opts RouteOpts) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		token := params["token"]

		if len(token) == 0 {
			log.Warn("failed to find the token parameter")
			respondWith.BadRequest("session_parameter_not_found").Send(w)
			return
		}
		db := opts.DatabaseConnection
		sessionInstance, err := session.GetSessionByToken(session.GetSessionOpts{
			DatabaseConnection: db,
			Session:            &session.Session{Token: string(token)},
		})
		if err != nil {
			log.Warnf("failed to get session from database: %s", err)
			respondWith.InternalServerError("session_database_error").Send(w)
			return
		}
		if sessionInstance == nil {
			log.Warnf("failed to find a session with token '%s'", token)
			respondWith.NotFound("session_not_found").Send(w)
			return
		}

		userAgent := r.UserAgent()
		sessionInstance.UserAgent = &userAgent
		ipAddress := r.RemoteAddr
		sessionInstance.IPAddress = &ipAddress
		if err := session.UpdateSession(session.UpdateSessionOpts{
			DatabaseConnection: db,
			Session:            sessionInstance,
		}); err != nil {
			log.Warnf("failed to update session in database: %s", err)
			respondWith.InternalServerError("session_not_updated").Send(w)
			return
		}

		clientToken, err := sessionInstance.GetToken(*opts.SigningKey)
		if err != nil {
			log.Warnf("failed to get signed client token: %s", err)
			respondWith.InternalServerError("session_signing_failed").Send(w)
			return
		}

		redirectToURL, _ := url.Parse(opts.ClientBaseURL)
		redirectToURL.Path = routes.Gateway

		cookieID := session.DefaultCookieID
		clientURL, err := url.Parse(opts.ClientBaseURL)
		if err != nil {
			log.Warnf("failed to get a valid client base url: %s", err)
			respondWith.InternalServerError("session_client_url_wonky").Send(w)
			return
		}
		cookieDomain := clientURL.Hostname()
		if cookieDomain == "" {
			cookieDomain = session.DefaultCookieDomain
		}

		cookiePath := session.DefaultCookiePath
		cookieSecure := clientURL.Scheme == "https"
		log.Debugf("responding with cookie '%s' for domain '%s' at path '%s'", cookieID, cookieDomain, cookiePath)
		respondWith.TemporaryRedirect(redirectToURL.String()).Send(w, utils.HttpAddons{
			Cookies: []http.Cookie{
				{
					Name:     cookieID,
					Value:    string(clientToken),
					Domain:   cookieDomain,
					Path:     cookiePath,
					Secure:   cookieSecure,
					HttpOnly: true,
				},
			},
		})
	})
}
