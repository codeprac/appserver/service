package session

import (
	"crypto/rsa"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/types"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

var respondWith utils.HttpResponder

type RouteOpts struct {
	ClientBaseURL      string
	DatabaseConnection *gorm.DB
	SigningKey         *rsa.PrivateKey
	VerifyingKey       *rsa.PublicKey
}

func (o RouteOpts) Validate() error {
	errors := []string{}

	if len(o.ClientBaseURL) == 0 {
		errors = append(errors, "missing client base url")
	}
	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}
	if o.SigningKey == nil {
		errors = append(errors, "missing session signing key")
	}
	if o.VerifyingKey == nil {
		errors = append(errors, "missing session verification key")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate session route opts: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func RouteWith(router types.Router, opts RouteOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to add session router: %s", err)
	}
	respondWith = utils.HttpResponder{
		Source:      "session",
		ErrorLogger: utils.LogAs(log.Warnf),
	}
	sessionRouter := router.PathPrefix("/session").Subrouter()
	sessionRouter.Handle("", get(opts)).Methods(http.MethodGet)
	sessionRouter.Handle("/{token}", getToken(opts)).Methods(http.MethodGet)
	return nil
}
