package account

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/account/linked"
	"gitlab.com/codeprac/codeprac/internal/api/types"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

var respondWith utils.HttpResponder

type RouteOpts struct {
	DatabaseConnection *gorm.DB
}

func (o RouteOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options for /account/**: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func RouteWith(router types.Router, opts RouteOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to route /account/**: %s", err)
	}

	respondWith = utils.HttpResponder{
		Source:      "account",
		ErrorLogger: utils.LogAs(log.Warnf),
	}
	accountRouter := router.PathPrefix("/account").Subrouter()
	if err := linked.RouteWith(accountRouter, linked.RouteOpts{
		DatabaseConnection: opts.DatabaseConnection,
	}); err != nil {
		return fmt.Errorf("failed to add routes for /account/linked/**: %s", err)
	}

	accountRouter.Handle("", getGetAccountHandler(opts)).Methods(http.MethodGet)
	accountRouter.Handle("", getPutAccountHandler(opts)).Methods(http.MethodPut)

	return nil
}
