package account

import (
	"net/http"

	account "gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func getGetAccountHandler(opts RouteOpts) http.HandlerFunc {
	get := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		accountInstance, err := account.GetByUUID(account.GetOpts{
			DatabaseConnection: opts.DatabaseConnection,
			UUID:               authInfo.GetAccountUUID(),
		})
		if err != nil {
			respondWith.InternalServerError("account_database_get_failed").Send(w)
			return
		}
		if accountInstance == nil {
			respondWith.NotFound("account_not_found").Send(w)
			return
		}

		linkedAccountInstances, err := account.GetLinkedAccounts(account.GetLinkedAccountsOpts{
			DatabaseConnection: opts.DatabaseConnection,
			AccountUUID:        authInfo.GetAccountUUID(),
		})
		if err != nil {
			respondWith.InternalServerError("account_linked_database_get_failed").Send(w)
			return
		}

		accountInstance.LinkedAccounts = linkedAccountInstances
		respondWith.OK(accountInstance).Send(w)
	})

	return get
}
