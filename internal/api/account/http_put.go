package account

import (
	"encoding/json"
	"io"
	"net/http"

	account "gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func getPutAccountHandler(opts RouteOpts) http.HandlerFunc {
	put := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		requestBody, err := io.ReadAll(r.Body)
		if err != nil {
			respondWith.BadRequest("account_invalid_body").Send(w)
			return
		}

		var serializedStructure PutAccountRequestSerializer
		if err := json.Unmarshal(requestBody, &serializedStructure); err != nil {
			log.Warnf("failed to unmarshal data '%s': %s", string(requestBody), err)
			respondWith.BadRequest("account_invalid_data").Send(w)
			return
		}
		serializedData, _ := json.Marshal(serializedStructure)
		accountPatch := account.Account{}
		if err := json.Unmarshal(serializedData, &accountPatch); err != nil {
			log.Warnf("failed to unmarshal data '%s': %s", string(requestBody), err)
			respondWith.InternalServerError("account_incompatible_serializer").Send(w)
			return
		}

		if err := account.Update(account.UpdateOpts{
			DatabaseConnection: opts.DatabaseConnection,
			UUID:               authInfo.GetAccountUUID(),
			Patch:              accountPatch,
		}); err != nil {
			log.Warnf("failed to update account with uuid '%s' in database: %s", authInfo.GetAccountUUID(), err)
			respondWith.InternalServerError("account_update_database").Send(w)
			return
		}

		respondWith.OK(accountPatch).Send(w)
	})

	return put
}
