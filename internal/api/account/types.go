package account

type PutAccountRequestSerializer struct {
	Email    *string `json:"email,omitempty"`
	Username *string `json:"username,omitempty"`
}
