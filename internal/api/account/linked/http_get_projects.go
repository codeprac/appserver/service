package linked

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	account "gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/constants"
	"gitlab.com/codeprac/codeprac/internal/github"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func getProjects(opts RouteOpts) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		vars := mux.Vars(r)
		linkedAccountUUID := vars["uuid"]
		linkedAccount, err := account.GetLinkedAccountByUUID(account.GetLinkedAccountOpts{
			DatabaseConnection: opts.DatabaseConnection,
			UUID:               linkedAccountUUID,
		})
		if err != nil {
			log.Warnf("failed to get linked account with uuid '%s' for account with id '%s': %s", linkedAccountUUID, authInfo.GetAccountUUID(), err)
			respondWith.InternalServerError("project_github_db").Send(w)
			return
		}
		if linkedAccount == nil {
			respondWith.NotFound("project_github").Send(w)
			return
		}

		page, err := strconv.Atoi(r.FormValue("page"))
		if err != nil {
			page = DefaultProjectPage
		}
		platform := r.FormValue("platform")
		limit, err := strconv.Atoi(r.FormValue("limit"))
		if err != nil {
			limit = DefaultProjectsLimit
		}

		switch platform {
		case constants.PlatformGithub:
			log.Infof("retrieving %v repositories on page %v from %s", limit, page, platform)
			repos, err := github.GetAuthenticatedUserRepos(linkedAccount.AccessToken, page, limit)
			if err != nil {
				log.Warnf("failed to get repos from github for account with uuid '%s': %s", authInfo.GetAccountUUID(), err)
				respondWith.InternalServerError("project_github_api").Send(w)
				return
			}
			respondWith.OK(repos).Send(w)
		default:
			respondWith.BadRequest("project_platform_invalid").Send(w)
			return
		}
	})
}
