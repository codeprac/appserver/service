package linked

import (
	"net/http"

	account "gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func get(opts RouteOpts) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		linkedAccountInstances, err := account.GetLinkedAccounts(account.GetLinkedAccountsOpts{
			DatabaseConnection: opts.DatabaseConnection,
			AccountUUID:        authInfo.GetAccountUUID(),
		})

		if err != nil {
			respondWith.InternalServerError("account_linked_database_get_failed").Send(w)
			return
		}
		if linkedAccountInstances == nil {
			respondWith.NotFound("account_linked_not_found").Send(w)
			return
		}

		respondWith.OK(linkedAccountInstances).Send(w)
	})
}
