package linked

const (
	DefaultProjectPage     = 0
	DefaultProjectPlatform = "github"
	DefaultProjectsLimit   = 20
)
