package project

import (
	"fmt"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/project/github"
	"gitlab.com/codeprac/codeprac/internal/api/types"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

var respondWith utils.HttpResponder

type RouteOpts struct {
	DatabaseConnection *gorm.DB
}

func (o RouteOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate project route opts: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func RouteWith(router types.Router, opts RouteOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to add project router: %s", err)
	}
	respondWith = utils.HttpResponder{
		Source:      "project",
		ErrorLogger: utils.LogAs(log.Warnf),
	}
	projectRouter := router.PathPrefix("/project").Subrouter()
	if err := github.RouteWith(projectRouter, github.RouteOpts{
		DatabaseConnection: opts.DatabaseConnection,
	}); err != nil {
		return fmt.Errorf("failed to register /projects/github/**: %s", err)
	}
	return nil
}
