package github

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/constants"
	"gitlab.com/codeprac/codeprac/internal/github"
	"gitlab.com/codeprac/codeprac/internal/project"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func get(opts RouteOpts) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}

		if _, err := uuid.Parse(authInfo.GetAccountUUID()); err != nil {
			respondWith.BadRequest("session_not_found").Send(w)
			return
		}

		linkedAccounts, err := account.GetLinkedAccountsFromPlatform(account.GetLinkedAccountsOpts{
			DatabaseConnection: opts.DatabaseConnection,
			AccountUUID:        authInfo.GetAccountUUID(),
			PlatformID:         constants.PlatformGithub,
		})
		if err != nil {
			log.Warnf("failed to get linked account for account with uuid '%s': %s", authInfo.GetAccountUUID(), err)
			respondWith.InternalServerError("project_github_db").Send(w)
			return
		}
		if linkedAccounts == nil {
			respondWith.NotFound("project_github_account_not_found").Send(w)
			return
		}

		linkedAccount := linkedAccounts[0]
		vars := mux.Vars(r)
		owner, path := vars["owner"], vars["path"]
		repoInfo, err := github.GetRepositoryInfo(linkedAccount.AccessToken, owner, path)
		if err != nil {
			log.Warnf("failed to get repository '%s/%s': %s", owner, path, err)
			respondWith.InternalServerError("project_github_github_api").Send(w)
			return
		}

		projectInstance, err := project.Get(project.GetOpts{
			DatabaseConnection: opts.DatabaseConnection,
			PlatformID:         constants.PlatformGithub,
			PlatformProjectID:  repoInfo.ID,
		})
		if err != nil {
			respondWith.InternalServerError("project_github_db_get").Send(w)
			return
		}

		if projectInstance == nil {
			projectInstance = &project.Project{
				SourceLinkedAccountUUID: linkedAccount.UUID,
				SourceLinkedAccount:     &linkedAccount,
				PlatformID:              constants.PlatformGithub,
			}
			if err := copyRepositoryIntoProject(repoInfo, projectInstance); err != nil {
				log.Warnf("failed to set project data given repo data: %s", err)
				respondWith.InternalServerError("project_github_data_create").Send(w)
				return
			}

			if projectInstance, err = project.Create(project.CreateOpts{
				DatabaseConnection: opts.DatabaseConnection,
				Project:            projectInstance,
			}); err != nil {
				respondWith.InternalServerError("project_github_db_create").Send(w)
				return
			}
		} else {
			if err = copyRepositoryIntoProject(repoInfo, projectInstance); err != nil {
				log.Warnf("failed to set project data given repo data: %s", err)
				respondWith.InternalServerError("project_github_data_create").Send(w)
				return
			}

			if err = project.Update(project.UpdateOpts{
				DatabaseConnection: opts.DatabaseConnection,
				Project:            projectInstance,
			}); err != nil {
				respondWith.InternalServerError("project_github_db_update").Send(w)
				return
			}
		}

		respondWith.OK(projectInstance).Send(w)
	})
}

func copyRepositoryIntoProject(repo *github.RepoInfo, proj *project.Project) error {
	marshalledRepo, err := json.Marshal(repo)
	if err != nil {
		return fmt.Errorf("failed to marshal github repository info: %s", err)
	}
	name := strings.Split(repo.FullName, "/")
	proj.Owner, proj.Path = name[0], strings.Join(name[1:], "/")
	proj.Name = repo.Name
	proj.Description = repo.Description
	proj.URL = repo.URL
	proj.CloneHttpURL = repo.CloneHttpURL
	proj.CloneSshURL = repo.CloneSshURL
	proj.FollowsCount = repo.Watchers
	proj.LikesCount = repo.Stars
	proj.PlatformProjectID = repo.ID
	proj.PlatformData = string(marshalledRepo)

	return nil
}
