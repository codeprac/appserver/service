package api

import (
	"crypto/rsa"
	"fmt"
	"net/url"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gorm.io/gorm"
)

var respondWith utils.HttpResponder

type APIOpts struct {
	ClientBaseURL       string `json:"clientBaseURL"`
	DatabaseConnection  *gorm.DB
	GithubClientID      string `json:"githubClientID"`
	GithubClientSecret  string `json:"githubClientSecret"`
	GithubRedirectURI   string `json:"githubRedirectURI"`
	SessionSigningKey   rsa.PrivateKey
	SessionVerifyingKey rsa.PublicKey
	TelegramToken       string `json:"telegramToken"`
}

func (o APIOpts) Validate() error {
	errors := []string{}

	if len(o.ClientBaseURL) == 0 {
		errors = append(errors, "missing client url")
	} else if _, err := url.Parse(o.ClientBaseURL); err != nil {
		errors = append(errors, fmt.Sprintf("invalid client url: %s", err))
	}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	} else if o.DatabaseConnection.Error != nil {
		errors = append(errors, fmt.Sprintf("invalid database connection: %s", o.DatabaseConnection.Error))
	}

	if len(o.GithubClientID) == 0 {
		errors = append(errors, "missing github client id")
	}

	if len(o.GithubClientSecret) == 0 {
		errors = append(errors, "missing github client secret")
	}

	if len(o.GithubRedirectURI) == 0 {
		errors = append(errors, "missing github redirect uri")
	}

	if err := o.SessionSigningKey.Validate(); err != nil {
		errors = append(errors, fmt.Sprintf("invalid signing key: %s", err))
	}

	if len(o.TelegramToken) == 0 {
		errors = append(errors, "missing telegram token")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate api configuration: %s", strings.Join(errors, ", "))
	}

	return nil
}
