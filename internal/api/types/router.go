package types

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Routes []Route

type Route struct {
	Handler http.Handler
	Method  string
	Path    string
}

type Router interface {
	Handle(path string, handler http.Handler) *mux.Route
	Methods(methods ...string) *mux.Route
	PathPrefix(path string) *mux.Route
}
