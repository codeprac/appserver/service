package unauth

import (
	"net/http"
	"net/url"

	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/session"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gitlab.com/codeprac/codeprac/pkg/routes"
)

func get(opts RouteOpts) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authInfo, err := utils.GetAuthInfo(w, r, respondWith)
		if err != nil {
			log.Warn(err)
			return
		}
		log.Infof("account[%s] logged out", authInfo.GetAccountUUID())

		redirectToURL, _ := url.Parse(opts.ClientBaseURL)
		redirectToURL.Path = routes.Home
		respondWith.TemporaryRedirect(redirectToURL.String()).Send(w, utils.HttpAddons{
			Cookies: []http.Cookie{
				{
					Name:     session.DefaultCookieID,
					Value:    "",
					Domain:   session.DefaultCookieDomain,
					Path:     session.DefaultCookiePath,
					MaxAge:   0,
					Secure:   false,
					HttpOnly: true,
				},
			},
		})
	})
}
