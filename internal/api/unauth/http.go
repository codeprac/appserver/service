package unauth

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/codeprac/codeprac/internal/api/types"
	"gitlab.com/codeprac/codeprac/internal/api/utils"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gorm.io/gorm"
)

type RouteOpts struct {
	ClientBaseURL      string
	DatabaseConnection *gorm.DB
}

func (o RouteOpts) Validate() error {
	errors := []string{}

	if len(o.ClientBaseURL) == 0 {
		errors = append(errors, "missing client base url")
	}
	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate unauth route opts: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

var respondWith utils.HttpResponder

func RouteWith(router types.Router, opts RouteOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to add unauth router: %s", err)
	}
	respondWith = utils.HttpResponder{
		Source:      "unauth",
		ErrorLogger: utils.LogAs(log.Warnf),
	}
	unauthRouter := router.PathPrefix("/unauth").Subrouter()
	unauthRouter.Handle("", get(opts)).Methods(http.MethodGet)
	return nil
}
