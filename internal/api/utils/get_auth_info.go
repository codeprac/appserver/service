package utils

import (
	"errors"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/codeprac/codeprac/internal/session"
)

func GetAuthInfo(w http.ResponseWriter, r *http.Request, respondWith ...HttpResponder) (*session.TokenData, error) {
	shouldRespond := len(respondWith) > 0
	var responder HttpResponder
	if shouldRespond {
		responder = respondWith[0]
	}
	sessionInfo, err := session.GetSessionInfoFromRequest(r)
	if err != nil {
		if shouldRespond {
			responder.NotFound("session_not_found").Send(w)
		}
		return nil, errors.New("session details were not found")
	}
	if _, err := uuid.Parse(sessionInfo.GetAccountUUID()); err != nil {
		if shouldRespond {
			responder.BadRequest("session_not_found").Send(w)
		}
		return nil, errors.New("session details were invalid")
	}
	return sessionInfo, nil
}
