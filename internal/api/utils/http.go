package utils

import (
	"net/http"
)

type HttpResponder struct {
	Source      string
	ErrorLogger LogAs
}

func (r HttpResponder) BadRequest(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: Response{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusBadRequest,
		},
	}
}

func (r HttpResponder) Forbidden(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: Response{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusForbidden,
		},
	}
}

func (r HttpResponder) InternalServerError(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: Response{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusInternalServerError,
		},
	}
}

func (r HttpResponder) NotImplemented(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: Response{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusNotImplemented,
		},
	}
}

func (r HttpResponder) NotFound(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: Response{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusNotFound,
		},
	}
}

func (r HttpResponder) OK(data ...interface{}) HttpResponse {
	code := "ok"
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: Response{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusOK,
		},
	}
}

func (r HttpResponder) TemporaryRedirect(toThisURL string) HttpResponse {
	return HttpResponse{
		Source: r.Source,
		instance: Response{
			Headers: map[string][]string{
				"Location": []string{toThisURL},
			},
			HTTPCode: http.StatusTemporaryRedirect,
			IsEmpty:  true,
		},
	}
}

func (r HttpResponder) Unauthorized(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: Response{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusUnauthorized,
		},
	}
}

type HttpAddons struct {
	Cookies []http.Cookie
	Headers http.Header
}

type HttpResponse struct {
	instance    Response
	ErrorLogger LogAs
	Source      string
}

func (r HttpResponse) Send(w http.ResponseWriter, addons ...HttpAddons) {
	if len(addons) > 0 {
		for _, addon := range addons {
			r.instance.Cookies = append(r.instance.Cookies, addon.Cookies...)
			for key, values := range addon.Headers {
				for _, value := range values {
					r.instance.Headers.Add(key, value)
				}
			}
		}
	}
	logError := DefaultLogger
	if r.ErrorLogger != nil {
		logError = r.ErrorLogger
	}
	var source *string = nil
	if len(r.Source) > 0 {
		source = &r.Source
	}
	if err := r.instance.Send(w); err != nil {
		logError("failed to send response from '%s': ", *source, err)
	}
}
