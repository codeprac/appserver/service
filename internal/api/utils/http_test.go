package utils

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
)

type HttpResponderTests struct {
	suite.Suite
}

func TestHttpResponder(t *testing.T) {
	suite.Run(t, &HttpResponderTests{})
}

func (s HttpResponderTests) Test_BadRequest() {
	w := httptest.NewRecorder()
	HttpResponder{Source: "module"}.BadRequest("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusBadRequest, w.Result().StatusCode)
}

func (s HttpResponderTests) Test_Forbidden() {
	w := httptest.NewRecorder()
	HttpResponder{Source: "module"}.Forbidden("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusForbidden, w.Result().StatusCode)
}

func (s HttpResponderTests) Test_InternalServerError() {
	w := httptest.NewRecorder()
	HttpResponder{Source: "module"}.InternalServerError("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusInternalServerError, w.Result().StatusCode)
}

func (s HttpResponderTests) Test_NotImplemented() {
	w := httptest.NewRecorder()
	HttpResponder{Source: "module"}.NotImplemented("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusNotImplemented, w.Result().StatusCode)
}

func (s HttpResponderTests) Test_NotFound() {
	w := httptest.NewRecorder()
	HttpResponder{Source: "module"}.NotFound("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusNotFound, w.Result().StatusCode)
}

func (s HttpResponderTests) Test_OK() {
	w := httptest.NewRecorder()
	HttpResponder{Source: "module"}.OK("data").Send(w)
	expectedBody := `{"code":"ok","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusOK, w.Result().StatusCode)
}

func (s HttpResponderTests) Test_TemporaryRedirect() {
	expectedURL := "http://localhost:3000"
	w := httptest.NewRecorder()
	HttpResponder{Source: "module"}.TemporaryRedirect(expectedURL).Send(w)
	expectedBody := ""
	s.Equal(expectedBody, w.Body.String())
	s.Contains(w.Result().Header["Location"], expectedURL)
	s.Equal(http.StatusTemporaryRedirect, w.Result().StatusCode)
}

func (s HttpResponderTests) Test_Unauthorized() {
	w := httptest.NewRecorder()
	HttpResponder{Source: "module"}.Unauthorized("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusUnauthorized, w.Result().StatusCode)
}
