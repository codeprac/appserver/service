package utils

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
)

type ResponseTests struct {
	suite.Suite
}

func TestResponse(t *testing.T) {
	suite.Run(t, &ResponseTests{})
}

func (s ResponseTests) Test_Send_All() {
	code := "code"
	message := "message"
	headerKey := "Hello"
	headerValues := []string{"there", "world"}
	httpStatus := http.StatusTeapot

	res := Response{
		Code: &code,
		Cookies: []http.Cookie{
			{
				Name:  "cookie_1",
				Value: "jar 1",
			},
			{
				Name:  "cookie_2",
				Value: "jar 2",
			},
		},
		Data: map[string]interface{}{
			"bool":   true,
			"float":  3.142,
			"int":    1,
			"string": "string",
			"uint":   -1,
		},
		Headers: map[string][]string{
			headerKey: []string{headerValues[0]},
			headerKey: []string{headerValues[1]},
		},
		Message:  &message,
		HTTPCode: httpStatus,
	}
	var recorder = httptest.NewRecorder()
	responseWriter := http.ResponseWriter(recorder)
	err := res.Send(responseWriter)
	s.Nil(err)

	expectedBody := `{"code":"code","data":{"bool":true,"float":3.142,"int":1,"string":"string","uint":-1},"message":"message"}`
	s.Equal(expectedBody, recorder.Body.String())
	s.Len(recorder.Result().Cookies(), 2)
	s.Equal("cookie_1", recorder.Result().Cookies()[0].Name)
	s.Equal("jar 1", recorder.Result().Cookies()[0].Value)
	s.Equal("cookie_2", recorder.Result().Cookies()[1].Name)
	s.Equal("jar 2", recorder.Result().Cookies()[1].Value)
	s.Contains([][]string{{headerValues[0]}, {headerValues[1]}}, recorder.Result().Header[headerKey])
	s.Equal(httpStatus, recorder.Result().StatusCode)
}

func (s ResponseTests) Test_Send_InvalidData() {
	type X func()
	type x struct {
		X X
	}
	res := Response{Data: &x{}}
	var recorder = httptest.NewRecorder()
	err := res.Send(recorder)
	s.NotNil(err)
	s.Contains(err.Error(), "failed to jsonify")
}

func (s ResponseTests) Test_Send_InvalidSender() {
	res := Response{}
	err := res.Send("")
	s.NotNil(err)
	s.Contains(err.Error(), "failed to get valid sender")
}

func (s ResponseTests) Test_Send_Minimal() {
	res := Response{}
	var recorder = httptest.NewRecorder()
	err := res.Send(recorder)
	s.Nil(err)

	expectedBody := `{"code":null,"data":null}`
	s.Equal(expectedBody, recorder.Body.String())
	s.Equal(http.StatusOK, recorder.Result().StatusCode)
}
