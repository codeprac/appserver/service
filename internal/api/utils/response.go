package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Response struct {
	Code     *string       `json:"code"`
	Cookies  []http.Cookie `json:"-"`
	Data     interface{}   `json:"data"`
	Headers  http.Header   `json:"-"`
	Message  *string       `json:"message,omitempty"`
	HTTPCode int           `json:"-"`
	IsEmpty  bool          `json:"-"`
}

func (r Response) Send(sender interface{}) error {
	switch sender.(type) {
	case http.ResponseWriter:
		send, ok := sender.(http.ResponseWriter)
		if !ok {
			return fmt.Errorf("failed to get a valid http.ResponseWriter")
		}

		if r.Cookies != nil && len(r.Cookies) > 0 {
			for _, cookie := range r.Cookies {
				http.SetCookie(send, &cookie)
			}
		}

		if r.Headers != nil {
			for key, values := range r.Headers {
				for _, value := range values {
					send.Header().Add(key, value)
				}
			}
		}

		httpCode := http.StatusOK
		if r.HTTPCode > 0 {
			httpCode = r.HTTPCode
		}
		send.WriteHeader(httpCode)

		responseBody := []byte{}
		if !r.IsEmpty {
			var err error
			responseBody, err = json.Marshal(r)
			if err != nil {
				return fmt.Errorf("failed to jsonify: %s", err)
			}
		}
		send.Write(responseBody)
	default:
		return fmt.Errorf("failed to get valid sender")
	}
	return nil
}
