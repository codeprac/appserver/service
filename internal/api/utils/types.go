package utils

// DefaultLogger is the default logger, to change it, set it with
// `response.DefaultLogger = ...`
var DefaultLogger LogAs = func(_ string, _ ...interface{}) {}

type LogAs func(string, ...interface{})
