package test

import (
	"database/sql/driver"

	"github.com/DATA-DOG/go-sqlmock"
)

// GenerateSQLMockArguments is a convenience function for creating a
// slice of driver.Value instances which accept any input, this reduces
// the inconvenience of having to specify exact values for all arguments
// passed into the .WithArgs method. Modify the required argument indicies
// as required before passing to .WithArgs
func GenerateSQLMockArguments(length int) []driver.Value {
	args := make([]driver.Value, length)
	for i := 0; i < length; i++ {
		args[i] = sqlmock.AnyArg()
	}
	return args
}
