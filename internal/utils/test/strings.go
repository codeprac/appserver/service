package test

import (
	"math/rand"
	"time"
)

type Charset string

const (
	CharsetAlphaLower        Charset = "abcdefghijklmnopqrstuvwxyz"
	CharsetAlphaUpper        Charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	CharsetNumeric           Charset = "0123456789"
	CharsetSpecial           Charset = "`~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?"
	CharsetAlphanumericLower Charset = CharsetAlphaLower + CharsetNumeric
	CharsetAlphanumericUpper Charset = CharsetAlphaUpper + CharsetNumeric
	CharsetAlphanumeric      Charset = CharsetAlphaLower + CharsetAlphaUpper + CharsetNumeric
	CharsetTypeable          Charset = CharsetAlphaLower + CharsetAlphaUpper + CharsetNumeric + CharsetSpecial
)

func GenerateRandomString(length int, charset Charset) string {
	output := make([]byte, length)
	rand.Seed(time.Now().Unix())
	for index := range output {
		output[index] = charset[rand.Intn(len(charset)-1)]
	}
	return string(output)
}
