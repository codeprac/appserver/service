package test

import (
	"database/sql"

	"github.com/DATA-DOG/go-sqlmock"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// asserter provides a blueprint for what we need to be assertive
type asserter interface {
	Nil(interface{}, ...interface{}) bool
}

// GenerateGormSQLMockSetup is a convenience function to setup the
// gorm database object using sqlmock. This is creates a boilerplate
// and should ideally be called in BeforeTest. Remember to call the
// .Close method in an AfterTest block
func GenerateGormSQLMockSetup(assert asserter) (*sql.DB, sqlmock.Sqlmock, *gorm.DB) {
	rawDatabaseConnection, mockController, err := sqlmock.New()
	assert.Nil(err)
	gormConnection, err := gorm.Open(postgres.New(postgres.Config{
		Conn:                 rawDatabaseConnection,
		PreferSimpleProtocol: true,
	}), &gorm.Config{})
	assert.Nil(err)
	return rawDatabaseConnection, mockController, gormConnection
}
