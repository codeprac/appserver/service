package test

import (
	"math/rand"
	"time"
)

func GenerateRandomUint() uint32 {
	rand.Seed(time.Now().Unix())
	return rand.Uint32()
}
