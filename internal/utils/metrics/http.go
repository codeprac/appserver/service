package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const DefaultHttpMetricsPath = "/metrics"

type NewHttpPrometheusMetricsOpts struct {
	Path string `json:"path"`
}

func NewHttpPrometheusMetrics(opts ...NewHttpPrometheusMetricsOpts) *HttpPrometheusMetrics {
	path := DefaultHttpMetricsPath
	if len(opts) > 0 && len(opts[0].Path) > 0 {
		path = opts[0].Path
	}

	return &HttpPrometheusMetrics{path: path}
}

type HttpPrometheusMetrics struct {
	path string
}

func (m HttpPrometheusMetrics) GetHttpPath() string {
	return m.path
}

func (m HttpPrometheusMetrics) GetHttpHandlerFunc() http.HandlerFunc {
	return promhttp.Handler().ServeHTTP
}
