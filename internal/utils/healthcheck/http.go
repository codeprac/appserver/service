package healthcheck

import (
	"encoding/json"
	"net/http"
)

const (
	CodeOK                     = "ok"
	CodeFaied                  = "healthcheck_failed"
	DefaultHttpHealthcheckPath = "/healthcheck"
)

type HttpResponse struct {
	Code string      `json:"code"`
	Data interface{} `json:"data"`
}

type HttpProbeHandler func(http.Request) error

type NewHttpHealthcheckOpts struct {
	Checks []HttpProbeHandler
	Path   string `json:"path"`
}

func NewHttpHealthcheck(opts ...NewHttpHealthcheckOpts) *HttpHealthcheck {
	checks := []HttpProbeHandler{}
	if len(opts) > 0 && opts[0].Checks != nil && len(opts[0].Checks) > 0 {
		checks = opts[0].Checks
	}
	path := DefaultHttpHealthcheckPath
	if len(opts) > 0 && len(opts[0].Path) > 0 {
		path = opts[0].Path
	}
	return &HttpHealthcheck{
		checks: checks,
		path:   path,
	}
}

type HttpHealthcheck struct {
	checks []HttpProbeHandler
	path   string
}

func (hc *HttpHealthcheck) AddCheck(handler HttpProbeHandler) {
	if hc.checks == nil {
		hc.checks = []HttpProbeHandler{}
	}
	hc.checks = append(hc.checks, handler)
}

func (hc HttpHealthcheck) GetHttpPath() string {
	return hc.path
}

func (hc HttpHealthcheck) GetHttpHandlerFunc() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		errors := hc.Run(*r)
		if errors != nil {
			w.WriteHeader(http.StatusInternalServerError)
			messages := []string{}
			for _, err := range errors {
				messages = append(messages, err.Error())
			}
			response := HttpResponse{Code: CodeFaied, Data: messages}
			jsonBody, _ := json.Marshal(response)
			w.Write(jsonBody)
			return
		}
		w.WriteHeader(http.StatusOK)
		response := HttpResponse{Code: CodeOK}
		jsonBody, _ := json.Marshal(response)
		w.Write(jsonBody)
	}
}

func (hc HttpHealthcheck) Run(requestInfo http.Request) []error {
	errors := []error{}
	for _, check := range hc.checks {
		if err := check(requestInfo); err != nil {
			errors = append(errors, err)
		}
	}
	if len(errors) == 0 {
		return nil
	}
	return errors
}
