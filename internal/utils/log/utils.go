package log

import (
	"path"
	"runtime"
)

func PrettifyCaller(frame *runtime.Frame) (function string, file string) {
	function = path.Base(frame.Function)
	file = path.Base(frame.File)
	return
}
