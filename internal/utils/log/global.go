package log

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
)

var Logger = logrus.New()

func init() {
	Logger.SetLevel(logrus.TraceLevel)
	Logger.SetOutput(os.Stderr)
	Logger.SetReportCaller(true)
	Logger.SetFormatter(&DefaultFormatter)
}

var Print, Trace, Debug, Info, Warn, Error SimpleLog = print, Logger.Trace, Logger.Debug, Logger.Info, Logger.Warn, Logger.Error
var Printf, Tracef, Debugf, Infof, Warnf, Errorf FormattedLog = printf, Logger.Tracef, Logger.Debugf, Logger.Infof, Logger.Warnf, Logger.Errorf

func print(args ...interface{}) {
	fmt.Print(args...)
}

func printf(message string, args ...interface{}) {
	fmt.Printf(message, args...)
}

var DefaultFormatter = logrus.TextFormatter{
	CallerPrettyfier:       PrettifyCaller,
	FullTimestamp:          true,
	QuoteEmptyFields:       true,
	DisableSorting:         true,
	DisableLevelTruncation: false,
}
