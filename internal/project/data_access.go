package project

import (
	"errors"
	"fmt"
	"strings"

	"gorm.io/gorm"
)

type CreateOpts struct {
	DatabaseConnection *gorm.DB
	Project            *Project
}

func (o CreateOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.Project == nil {
		errors = append(errors, "missing project instance")
	} else if o.Project.ID != 0 || o.Project.UUID != "" {
		errors = append(errors, "project already has an identifier")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for creating a project: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func Create(opts CreateOpts) (*Project, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to create project: %s", err)
	}
	projectInstance := *opts.Project
	query := opts.DatabaseConnection.Create(&projectInstance)
	if query.Error != nil {
		return nil, fmt.Errorf("failed to insert project with url '%s' into database: %s", opts.Project.URL, query.Error)
	}
	return &projectInstance, nil
}

type GetOpts struct {
	DatabaseConnection *gorm.DB
	PlatformID         string
	PlatformProjectID  string
}

func (o GetOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.PlatformID == "" {
		errors = append(errors, "missing platform id")
	}
	if o.PlatformProjectID == "" {
		errors = append(errors, "missing platform project id")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for getting a project: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func Get(opts GetOpts) (*Project, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to get project: %s", err)
	}
	projectInstance := Project{
		PlatformID:        opts.PlatformID,
		PlatformProjectID: opts.PlatformProjectID,
	}
	query := opts.DatabaseConnection.First(&projectInstance, projectInstance)
	if query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to get project from database: %s", query.Error)
	}
	return &projectInstance, nil
}

type UpdateOpts struct {
	DatabaseConnection *gorm.DB
	Project            *Project
}

func (o UpdateOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.Project.ID == 0 {
		errors = append(errors, "missing project id")
	}

	if o.Project.UUID == "" {
		errors = append(errors, "missing project uuid")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options for updating project: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func Update(opts UpdateOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to validate options: %s", err)
	}
	query := opts.DatabaseConnection.Updates(opts.Project)
	if query.Error != nil {
		return fmt.Errorf("failed to update project with id %v and uuid '%s': %s", opts.Project.ID, opts.Project.UUID, query.Error)
	}
	return nil
}
