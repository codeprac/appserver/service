package project

import (
	"time"

	"gitlab.com/codeprac/codeprac/internal/account"
)

type Project struct {
	ID                      uint                   `json:"-" gorm:"primaryKey;autoIncrement"`
	UUID                    string                 `json:"uuid" gorm:"uniqueIndex;type:uuid;default:uuid_generate_v4()"`
	SourceLinkedAccountUUID string                 `json:"-" gorm:"type:uuid"`
	SourceLinkedAccount     *account.LinkedAccount `json:"-" gorm:"references:UUID"`
	Name                    string                 `json:"name"`
	Owner                   string                 `json:"owner"`
	Path                    string                 `json:"path"`
	Description             string                 `json:"description"`
	URL                     string                 `json:"url"`
	CloneHttpURL            string                 `json:"cloneHttpURL"`
	CloneSshURL             string                 `json:"cloneSshURL"`
	FollowsCount            int                    `json:"followsCount"`
	LikesCount              int                    `json:"likesCount"`
	PlatformID              string                 `json:"platformID"`
	PlatformProjectID       string                 `json:"platformProjectID"`
	PlatformData            string                 `json:"platformData"`
	CreatedAt               time.Time              `json:"-"`
	UpdatedAt               time.Time              `json:"-"`
	DeletedAt               time.Time              `json:"-"`
}
