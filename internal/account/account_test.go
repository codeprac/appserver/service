package account

import (
	"database/sql"
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/codeprac/codeprac/internal/utils/test"
	"gorm.io/gorm"
)

type AccountTests struct {
	suite.Suite
	db         *sql.DB
	mock       sqlmock.Sqlmock
	connection *gorm.DB

	string1 string
	string2 string
	uint1   uint32
	uuid1   string
	uuid2   string
}

func TestAccount(t *testing.T) {
	suite.Run(t, &AccountTests{})
}

func (s *AccountTests) AfterTest(_, _ string) {
	s.db.Close()
}

func (s *AccountTests) BeforeTest(_, _ string) {
	s.db, s.mock, s.connection = test.GenerateGormSQLMockSetup(s)
	s.string1 = test.GenerateRandomString(16, test.CharsetAlphanumericLower)
	s.string2 = test.GenerateRandomString(12, test.CharsetAlphanumericLower)
	s.uint1 = test.GenerateRandomUint()
	s.uuid1 = uuid.New().String()
	s.uuid2 = uuid.New().String()
}

func (s AccountTests) Test_Create() {
	expectedUsername := s.string1
	expectedEmail := s.string1 + "@" + s.string2 + ".com"

	args := test.GenerateSQLMockArguments(6)
	results := sqlmock.NewRows([]string{"uuid", "id"})
	s.mock.ExpectBegin()
	s.mock.ExpectQuery(`INSERT INTO "accounts"`).WithArgs(args...).WillReturnRows(results)
	s.mock.ExpectCommit()
	_, err := Create(CreateOpts{
		DatabaseConnection: s.connection,
		Account: Account{
			Email:    &expectedEmail,
			Username: &expectedUsername,
		},
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected a full account to be created")

	args = test.GenerateSQLMockArguments(6)
	results = sqlmock.NewRows([]string{"uuid", "id"})
	s.mock.ExpectBegin()
	s.mock.ExpectQuery(`INSERT INTO "accounts"`).WithArgs(args...).WillReturnRows(results)
	s.mock.ExpectCommit()
	_, err = Create(CreateOpts{
		DatabaseConnection: s.connection,
		Account:            Account{},
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected a null account to be created")
}

func (s AccountTests) Test_Get() {
	expectedEmail := s.string1 + "@" + s.string2 + ".com"

	args := test.GenerateSQLMockArguments(1)
	args[0] = expectedEmail
	results := &sqlmock.Rows{}
	s.mock.ExpectQuery(`SELECT \* FROM "accounts" WHERE.+"email" =.+`).WithArgs(args...).WillReturnRows(results)
	_, err := GetByEmail(GetOpts{
		DatabaseConnection: s.connection,
		Email:              expectedEmail,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected an account to be selected for using its email")

	args = test.GenerateSQLMockArguments(1)
	args[0] = s.uint1
	s.mock.ExpectQuery(`SELECT \* FROM "accounts" WHERE.+"id" =.+`).WithArgs(args...).WillReturnRows(results)
	_, err = GetByID(GetOpts{
		DatabaseConnection: s.connection,
		ID:                 uint(s.uint1),
	})
	fmt.Println(err)
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected an account to be selected for using its id")

	args = test.GenerateSQLMockArguments(1)
	args[0] = s.string1
	s.mock.ExpectQuery(`SELECT \* FROM "accounts" WHERE.+"username" =.+`).WithArgs(args...).WillReturnRows(results)
	_, err = GetByUsername(GetOpts{
		DatabaseConnection: s.connection,
		Username:           s.string1,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected an account to be selected for using its username")

	args = test.GenerateSQLMockArguments(1)
	args[0] = s.uuid1
	s.mock.ExpectQuery(`SELECT \* FROM "accounts" WHERE.+"uuid" =.+`).WithArgs(args...).WillReturnRows(results)
	_, err = GetByUUID(GetOpts{
		DatabaseConnection: s.connection,
		UUID:               s.uuid1,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected an account to be selected for using its username")
}

func (s AccountTests) Test_Update() {
	expectedUsername := s.string1
	expectedEmail := s.string1 + "@" + s.string2 + ".com"

	args := test.GenerateSQLMockArguments(5)
	args[0] = expectedEmail
	args[1] = expectedUsername
	args[2] = s.uuid2
	results := sqlmock.NewResult(0, 1)

	s.mock.ExpectBegin()
	s.mock.ExpectExec(`UPDATE "accounts" SET.+"email"=.+"username"=.+"primary_profile_uuid"=.+WHERE.+"uuid" =.+`).
		WithArgs(args...).
		WillReturnResult(results)
	s.mock.ExpectCommit()
	err := Update(UpdateOpts{
		DatabaseConnection: s.connection,
		UUID:               s.uuid1,
		Patch: Account{
			Email:              &expectedEmail,
			Username:           &expectedUsername,
			PrimaryProfileUUID: &s.uuid2,
		},
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected an account to be updated based on its uuid")
}
