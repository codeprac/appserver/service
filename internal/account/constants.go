package account

type ValidateKey string

const (
	ValidateEmail          ValidateKey = "email"
	ValidateID             ValidateKey = "id"
	ValidatePlatformID     ValidateKey = "platform_id"
	ValidatePlatformUserID ValidateKey = "platform_user_id"
	ValidateUsername       ValidateKey = "username"
	ValidateUUID           ValidateKey = "uuid"
)
