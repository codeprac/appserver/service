package account

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gorm.io/gorm"
)

// Account stores data essential to authentication/authorisation when using our
// system
type Account struct {
	ID                 uint            `json:"-" gorm:"primaryKey,autoIncrement"`
	UUID               string          `json:"uuid" gorm:"uniqueIndex;type:uuid;default:uuid_generate_v4()"`
	Email              *string         `json:"email" gorm:"type:varchar(256);unique"`
	LinkedAccounts     []LinkedAccount `json:"linkedAccounts" gorm:"foreignKey:AccountID"`
	Username           *string         `json:"username" gorm:"type:varchar(64);uniqueIndex"`
	PrimaryProfileUUID *string         `json:"primaryProfileUUID" gorm:"type:uuid"`
	CreatedAt          time.Time       `json:"-"`
	UpdatedAt          time.Time       `json:"-"`
	DeletedAt          time.Time       `json:"-"`
}

// CreateOpts provides a structure for the Create method
type CreateOpts struct {
	DatabaseConnection *gorm.DB
	Account            Account
}

// Validate returns an error if any of the required parameters are invalid/missing
func (o CreateOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for creating an account: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

// Create inserts the provided opts.Account into the database and returns
// a pointer to the created Account instance
func Create(opts CreateOpts) (*Account, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to create account: %s", err)
	}
	db := opts.DatabaseConnection
	accountInstance := opts.Account
	query := db.Create(&accountInstance)
	if query.Error != nil {
		return nil, fmt.Errorf("failed to insert account into database: %s", query.Error)
	}
	return &accountInstance, nil
}

// GetOpts provides a structure for the Get* method
type GetOpts struct {
	DatabaseConnection *gorm.DB
	Email              string
	ID                 uint
	Username           string
	UUID               string
}

// Validate returns an error if any of the required parameters are invalid/missing
func (o GetOpts) Validate(properties ...ValidateKey) error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(properties) > 0 {
		for _, property := range properties {
			switch property {
			case ValidateEmail:
				if len(o.Email) == 0 {
					errors = append(errors, "missing email address")
				}
			case ValidateUsername:
				if len(o.Username) == 0 {
					errors = append(errors, "missing username")
				}
			case ValidateUUID:
				if len(o.UUID) == 0 {
					errors = append(errors, "missing uuid")
				}
			case ValidateID:
				if o.ID == 0 {
					errors = append(errors, "missing id")
				}
			}

		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for retrieving an account: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

// GetByEmail retrieves the account instance by selecting for the email,
// if no records are found, returns a nil first argument
func GetByEmail(opts GetOpts) (*Account, error) {
	if err := opts.Validate(ValidateEmail); err != nil {
		return nil, fmt.Errorf("failed to validate options for retrieving account: %s", err)
	}
	accountInstance, err := get(opts.DatabaseConnection, Account{Email: &opts.Email})
	if err != nil {
		return nil, fmt.Errorf("failed to find for an account with email '%s': %s", opts.Email, err)
	}
	return accountInstance, nil
}

// GetByID retrieves the account instance by selecting for the account's id,
// if no records are found, returns a nil first argument
func GetByID(opts GetOpts) (*Account, error) {
	if err := opts.Validate(ValidateID); err != nil {
		return nil, fmt.Errorf("failed to validate options for retrieving account: %s", err)
	}
	accountInstance, err := get(opts.DatabaseConnection, Account{ID: opts.ID})
	if err != nil {
		return nil, fmt.Errorf("failed to find for an account with id %v: %s", opts.ID, err)
	}
	return accountInstance, nil
}

// GetByUsername retrieves the account instance by selecting for the account's
// username, if no records are found, returns a nil first argument
func GetByUsername(opts GetOpts) (*Account, error) {
	if err := opts.Validate(ValidateUsername); err != nil {
		return nil, fmt.Errorf("failed to validate options for retrieving account: %s", err)
	}
	accountInstance, err := get(opts.DatabaseConnection, Account{Username: &opts.Username})
	if err != nil {
		return nil, fmt.Errorf("failed to find for an account with username '%s': %s", opts.Username, err)
	}
	return accountInstance, nil
}

// GetByUUID retrieves the account instance by selecting for the account's
// UUID, if no records are found, returns a nil first argument
func GetByUUID(opts GetOpts) (*Account, error) {
	if err := opts.Validate(ValidateUUID); err != nil {
		return nil, fmt.Errorf("failed to validate options for retrieving account: %s", err)
	}
	accountInstance, err := get(opts.DatabaseConnection, Account{UUID: opts.UUID})
	if err != nil {
		return nil, fmt.Errorf("failed to find for an account with uuid '%s': %s", opts.UUID, err)
	}
	return accountInstance, nil
}

// get is the driver function behind the exported convenience functions
func get(connection *gorm.DB, search Account) (*Account, error) {
	accountInstance := Account{}
	if query := connection.First(&accountInstance, search); query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to find for an account into database: %s", query.Error)
	}
	return &accountInstance, nil
}

// UpdateOpts provides a structure for Update method arguments
type UpdateOpts struct {
	DatabaseConnection *gorm.DB
	UUID               string
	Patch              Account
}

// Validate returns an error if any required parmaeters are missing/invalid
func (o UpdateOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.UUID == "" {
		errors = append(errors, "missing account uuid")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options for patching an account: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

// Update updates an account identified by its UUID with the fields set in the provided patch
// Account instance
func Update(opts UpdateOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to patch account: %s", err)
	}
	search := Account{UUID: opts.UUID}
	if update := opts.DatabaseConnection.Model(&search).Where(search).Updates(opts.Patch); update.Error != nil {
		return fmt.Errorf("failed to patch account in database with uuid '%s': %s", opts.UUID, update.Error)
	}
	return nil
}
