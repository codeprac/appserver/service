package account

import (
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/codeprac/codeprac/internal/utils/test"
	"gorm.io/gorm"
)

type LinkedAccountTests struct {
	suite.Suite
	db         *sql.DB
	mock       sqlmock.Sqlmock
	connection *gorm.DB

	string1 string
	string2 string
	uint1   uint32
	uuid1   string
	uuid2   string
}

func TestLinkedAccount(t *testing.T) {
	suite.Run(t, &LinkedAccountTests{})
}

func (s *LinkedAccountTests) AfterTest(_, _ string) {
	s.db.Close()
}

func (s *LinkedAccountTests) BeforeTest(_, _ string) {
	s.db, s.mock, s.connection = test.GenerateGormSQLMockSetup(s)
	s.string1 = test.GenerateRandomString(16, test.CharsetAlphanumericLower)
	s.string2 = test.GenerateRandomString(12, test.CharsetAlphanumericLower)
	s.uint1 = test.GenerateRandomUint()
	s.uuid1 = uuid.New().String()
	s.uuid2 = uuid.New().String()
}

func (s LinkedAccountTests) Test_CreateLinkedAccount_null() {
	args := test.GenerateSQLMockArguments(10)
	results := sqlmock.NewRows([]string{"uuid", "id"})
	s.mock.ExpectBegin()
	s.mock.ExpectQuery(`INSERT INTO "linked_accounts"`).WithArgs(args...).WillReturnRows(results)
	s.mock.ExpectCommit()
	_, err := CreateLinkedAccount(CreateLinkedAccountOpts{
		DatabaseConnection: s.connection,
		LinkedAccount: LinkedAccount{
			AccessToken: s.string1,
		},
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected a null linked account to be created")
}

func (s LinkedAccountTests) Test_GetLinkedAccount() {
	args := test.GenerateSQLMockArguments(2)
	args[0] = s.string1
	args[1] = s.string2
	results := &sqlmock.Rows{}
	s.mock.ExpectQuery(`SELECT \* FROM "linked_accounts" WHERE.+"platform_id" =.+AND.+"platform_user_id" =.+`).WithArgs(args...).WillReturnRows(results)
	_, err := GetLinkedAccount(GetLinkedAccountOpts{
		DatabaseConnection: s.connection,
		PlatformID:         s.string1,
		PlatformUserID:     s.string2,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected an account to be selected for using its email")
}

func (s LinkedAccountTests) Test_GetLinkedAccounts() {
	expectedAccountID := 1
	args := test.GenerateSQLMockArguments(1)
	args[0] = s.uuid1
	s.mock.ExpectQuery(`SELECT \* FROM "accounts" WHERE.+"uuid" =.+`).
		WithArgs(args...).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(expectedAccountID)).
		WillReturnError(nil)
	args = test.GenerateSQLMockArguments(1)
	args[0] = expectedAccountID
	s.mock.ExpectQuery(`SELECT \* FROM "linked_accounts" WHERE.+"account_id" =.+`).
		WillReturnRows(&sqlmock.Rows{})
	_, err := GetLinkedAccounts(GetLinkedAccountsOpts{
		DatabaseConnection: s.connection,
		AccountUUID:        s.uuid1,
	})
	s.Nil(err)
	s.Nil(s.mock.ExpectationsWereMet(), "expected an account to be selected for using its email")
}
