package account

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gorm.io/gorm"
)

// LinkedAccount stores details about the user's account with an auth provider
// that grants them access to our system
type LinkedAccount struct {
	ID             uint      `json:"-" gorm:"primaryKey,autoIncrement"`
	UUID           string    `json:"uuid" gorm:"uniqueIndex;type:uuid;default:uuid_generate_v4()"`
	AccountID      uint      `json:"-"`
	PlatformID     string    `json:"platformID" gorm:"type:varchar(16)"`
	PlatformData   string    `json:"platformData"`
	PlatformUserID string    `json:"platformUserID" gorm:"type:varchar(64)"`
	AccessToken    string    `json:"-"`
	RefreshToken   string    `json:"-"`
	ExpiresAt      time.Time `json:"expiresAt"`
	CreatedAt      time.Time `json:"-"`
	UpdatedAt      time.Time `json:"-"`
	DeletedAt      time.Time `json:"-"`
}

// CreateLinkedAccountOpts is the option set for CreateLinkedAccount
type CreateLinkedAccountOpts struct {
	DatabaseConnection *gorm.DB
	LinkedAccount      LinkedAccount
}

// Validate returns nil if the option set is safe for using
func (o CreateLinkedAccountOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.LinkedAccount.ID != 0 {
		errors = append(errors, "id already defined (does it already exist?)")
	}

	if len(o.LinkedAccount.AccessToken) == 0 {
		errors = append(errors, "missing access token")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for creating a linked account: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

// CreateLinkedAccount inserts a new linked account and returns a pointer
// to a populated instance of LinkedAccount
func CreateLinkedAccount(opts CreateLinkedAccountOpts) (*LinkedAccount, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to create linked account: %s", err)
	}
	linkedAccount := opts.LinkedAccount
	if query := opts.DatabaseConnection.Create(&linkedAccount); query.Error != nil {
		return nil, fmt.Errorf("failed to insert linked account into database: %s", query.Error)
	}
	return &linkedAccount, nil
}

// GetLinkedAccountOpts provides a structure for the GetLinkedAccount method
type GetLinkedAccountOpts struct {
	DatabaseConnection *gorm.DB
	UUID               string
	// PlatformID identifies the platform
	PlatformID string
	// PlatformUserID identifies the user on the platform
	PlatformUserID string
}

// Validate returns an error if any of the required parameters are missing/invalid
func (o GetLinkedAccountOpts) Validate(keys ...ValidateKey) error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if len(keys) > 0 {
		for _, key := range keys {
			switch key {
			case ValidatePlatformID:
				if len(o.PlatformID) == 0 {
					errors = append(errors, "missing platform id")
				}
			case ValidatePlatformUserID:
				if len(o.PlatformUserID) == 0 {
					errors = append(errors, "missing platform user id")
				}
			case ValidateUUID:
				if len(o.UUID) == 0 {
					errors = append(errors, "missing uuid")
				}
			}
		}
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate opts for getting linked account: ['%s']", strings.Join(errors, "', '"))
	}
	return nil
}

func GetLinkedAccountByUUID(opts GetLinkedAccountOpts) (*LinkedAccount, error) {
	if err := opts.Validate(ValidateUUID); err != nil {
		return nil, fmt.Errorf("failed to get linked account: %s", err)
	}
	linkedAccountInstance := &LinkedAccount{}
	if query := opts.DatabaseConnection.Where(LinkedAccount{
		UUID: opts.UUID,
	}).First(&linkedAccountInstance); query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to find for a linked account in the database: %s", query.Error)
	}
	return linkedAccountInstance, nil
}

// GetLinkedAccount returns a LinkedAccount that matches the search
// parameters defined in the provided opts, if no records are found,
// returns a nil first argument
func GetLinkedAccount(opts GetLinkedAccountOpts) (*LinkedAccount, error) {
	if err := opts.Validate(ValidatePlatformID, ValidatePlatformUserID); err != nil {
		return nil, fmt.Errorf("failed to get linked account: %s", err)
	}
	linkedAccountInstance := &LinkedAccount{}
	if query := opts.DatabaseConnection.Where(LinkedAccount{
		PlatformID:     opts.PlatformID,
		PlatformUserID: opts.PlatformUserID,
	}).First(&linkedAccountInstance); query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to find for a linked account in the database: %s", query.Error)
	}
	return linkedAccountInstance, nil
}

type GetLinkedAccountsOpts struct {
	DatabaseConnection *gorm.DB
	AccountUUID        string
	PlatformID         string
}

func (o GetLinkedAccountsOpts) Validate(keys ...ValidateKey) error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.AccountUUID == "" {
		errors = append(errors, "missing account uuid")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options for retrieving linked accounts: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func GetLinkedAccountsFromPlatform(opts GetLinkedAccountsOpts) ([]LinkedAccount, error) {
	if err := opts.Validate(ValidatePlatformID); err != nil {
		return nil, fmt.Errorf("failed to validate options: %s", err)
	}

	account := Account{UUID: opts.AccountUUID}
	if query := opts.DatabaseConnection.First(&account, account); query.Error != nil {
		return nil, fmt.Errorf("failed to get account with uuid '%s': %s", opts.AccountUUID, query.Error)
	}
	linkedAccount := LinkedAccount{AccountID: account.ID, PlatformID: opts.PlatformID}
	var linkedAccountInstances []LinkedAccount
	if query := opts.DatabaseConnection.Find(&linkedAccountInstances, linkedAccount); query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve an account with uuid '%s': %s", opts.AccountUUID, query.Error)
	}

	return linkedAccountInstances, nil
}

func GetLinkedAccounts(opts GetLinkedAccountsOpts) ([]LinkedAccount, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to validate options: %s", err)
	}

	account := Account{UUID: opts.AccountUUID}
	if query := opts.DatabaseConnection.First(&account, account); query.Error != nil {
		return nil, fmt.Errorf("failed to get account with uuid '%s': %s", opts.AccountUUID, query.Error)
	}
	linkedAccount := LinkedAccount{AccountID: account.ID}
	var linkedAccountInstances []LinkedAccount
	if query := opts.DatabaseConnection.Find(&linkedAccountInstances, linkedAccount); query.Error != nil {
		if errors.Is(query.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve an account with uuid '%s': %s", opts.AccountUUID, query.Error)
	}

	return linkedAccountInstances, nil
}

type UpdateLinkedAccountOpts struct {
	DatabaseConnection *gorm.DB
	UUID               string
	Patch              LinkedAccount
}

// Validate returns an error if any required parmaeters are missing/invalid
func (o UpdateLinkedAccountOpts) Validate() error {
	errors := []string{}

	if o.DatabaseConnection == nil {
		errors = append(errors, "missing db connection")
	}

	if o.UUID == "" {
		errors = append(errors, "missing linked account uuid")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options for patching an account: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func UpdateLinkedAccount(opts UpdateLinkedAccountOpts) error {
	if err := opts.Validate(); err != nil {
		return fmt.Errorf("failed to patch linked account: %s", err)
	}
	search := LinkedAccount{UUID: opts.UUID}
	if update := opts.DatabaseConnection.Model(&search).Where(search).Updates(opts.Patch); update.Error != nil {
		return fmt.Errorf("failed to patch linked account in database with uuid '%s': %s", opts.UUID, update.Error)
	}
	return nil
}
