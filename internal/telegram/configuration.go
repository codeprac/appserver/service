package telegram

import "github.com/usvc/go-config"

const (
	ConfigKeyToken = "telegram-token"
)

func GetConfiguration() config.Map {
	return config.Map{
		ConfigKeyToken: &config.String{
			Usage: "Telegram API token from the @BotFather",
		},
	}
}
