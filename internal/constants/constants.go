package constants

var (
	PlatformGithub   string = "github"
	PlatformGitlab   string = "gitlab"
	PlatformTelegram string = "telegram"
	TokenCharacters  string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	Version          string = ""
)
