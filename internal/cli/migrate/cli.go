package migrate

import (
	"github.com/spf13/cobra"
	"gitlab.com/codeprac/codeprac/internal/cli/migrate/database"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "migrate",
		Aliases: []string{"m"},
		RunE: func(cmd *cobra.Command, args []string) error {
			return cmd.Help()
		},
	}
	command.AddCommand(database.GetCommand())
	return command
}
