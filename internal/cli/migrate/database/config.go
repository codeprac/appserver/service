package database

import (
	"github.com/usvc/go-config"
	"gitlab.com/codeprac/codeprac/internal/database"
)

var databaseConfig = database.GetConfiguration()

var conf = config.Map{}

func init() {
	// aggregates the different configurations
	for key, value := range databaseConfig {
		conf[key] = value
	}
}
