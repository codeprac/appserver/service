package database

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/codeprac/codeprac/internal/account"
	"gitlab.com/codeprac/codeprac/internal/database"
	"gitlab.com/codeprac/codeprac/internal/database/connection"
	"gitlab.com/codeprac/codeprac/internal/database/migration"
	"gitlab.com/codeprac/codeprac/internal/profile"
	"gitlab.com/codeprac/codeprac/internal/project"
	"gitlab.com/codeprac/codeprac/internal/session"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "database",
		Aliases: []string{"db"},
		RunE: func(cmd *cobra.Command, args []string) error {
			postgresConnectionOpts := connection.NewPostgresConnectionOpts{
				Host:     conf.GetString(database.ConfigKeyHost),
				Port:     uint16(conf.GetInt(database.ConfigKeyPort)),
				User:     conf.GetString(database.ConfigKeyUser),
				Password: conf.GetString(database.ConfigKeyPassword),
				Database: conf.GetString(database.ConfigKeyDatabase),
				SSLMode:  conf.GetBool(database.ConfigKeySSLMode),
				Logger: &connection.Logger{
					InfoLevel:  connection.FormattedLogger(log.Infof),
					WarnLevel:  connection.FormattedLogger(log.Warnf),
					ErrorLevel: connection.FormattedLogger(log.Errorf),
				},
			}
			log.Infof(
				"connecting to %s@%s:%v/%s (password: %s)",
				postgresConnectionOpts.User,
				postgresConnectionOpts.Host,
				postgresConnectionOpts.Port,
				postgresConnectionOpts.Database,
				postgresConnectionOpts.GetPasswordDefinedStatus(),
			)
			connection, err := connection.NewPostgresConnection(postgresConnectionOpts)
			if err != nil {
				return fmt.Errorf("failed to create a new connection: %s", err)
			}

			models := account.Models
			models = append(models, profile.Models...)
			models = append(models, project.Models...)
			models = append(models, session.Models...)

			if errs := migration.Run(migration.RunOpts{
				Connection:  connection,
				ErrorLogger: migration.FormattedLog(log.Errorf),
				Logger:      migration.FormattedLog(log.Debugf),
				Models:      models,
			}); errs != nil {
				messages := []string{}
				for _, err := range errs {
					messages = append(messages, err.Error())
				}
				message := fmt.Sprintf("failed to complete migrations: ['%s']", strings.Join(messages, "', '"))
				log.Error(message)
				return fmt.Errorf(message)
			}
			log.Infof("migrations completed successfully")
			return nil
		},
	}
	conf.ApplyToCobra(command)
	return command
}
