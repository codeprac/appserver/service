package server

import (
	"github.com/usvc/go-config"
	"gitlab.com/codeprac/codeprac/internal/client"
	"gitlab.com/codeprac/codeprac/internal/database"
	"gitlab.com/codeprac/codeprac/internal/github"
	"gitlab.com/codeprac/codeprac/internal/server"
	"gitlab.com/codeprac/codeprac/internal/session"
	"gitlab.com/codeprac/codeprac/internal/telegram"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
)

var configSets = []config.Map{
	client.GetConfiguration(),
	database.GetConfiguration(),
	github.GetConfiguration(),
	server.GetConfiguration(),
	session.GetConfiguration(),
	telegram.GetConfiguration(),
}

var conf = config.Map{}

func init() {
	for _, configSet := range configSets {
		for key, value := range configSet {
			if _, ok := conf[key]; ok {
				log.Warnf("configuration '%s' was already defined but has been overwritten", key)
			}
			conf[key] = value
		}
	}
}
