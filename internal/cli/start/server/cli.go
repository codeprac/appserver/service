package server

import (
	"context"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/codeprac/codeprac/internal/api"
	"gitlab.com/codeprac/codeprac/internal/client"
	"gitlab.com/codeprac/codeprac/internal/database"
	"gitlab.com/codeprac/codeprac/internal/database/connection"
	"gitlab.com/codeprac/codeprac/internal/github"
	"gitlab.com/codeprac/codeprac/internal/server"
	"gitlab.com/codeprac/codeprac/internal/session"
	"gitlab.com/codeprac/codeprac/internal/telegram"
	"gitlab.com/codeprac/codeprac/internal/utils/healthcheck"
	"gitlab.com/codeprac/codeprac/internal/utils/log"
	"gitlab.com/codeprac/codeprac/internal/utils/metrics"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "server",
		Aliases: []string{"s"},
		RunE:    run,
	}
	conf.ApplyToCobra(command)
	return command
}

func run(cmd *cobra.Command, args []string) error {

	// database

	log.Debugf("setting up database connection...")
	postgresConnectionOpts := connection.NewPostgresConnectionOpts{
		Host:     conf.GetString(database.ConfigKeyHost),
		Port:     uint16(conf.GetInt(database.ConfigKeyPort)),
		User:     conf.GetString(database.ConfigKeyUser),
		Password: conf.GetString(database.ConfigKeyPassword),
		Database: conf.GetString(database.ConfigKeyDatabase),
		SSLMode:  conf.GetBool(database.ConfigKeySSLMode),
		Logger: &connection.Logger{
			InfoLevel:  connection.FormattedLogger(log.Infof),
			WarnLevel:  connection.FormattedLogger(log.Warnf),
			ErrorLevel: connection.FormattedLogger(log.Errorf),
		},
	}
	log.Debugf(
		"connecting to %s@%s:%v/%s (password: %s)...",
		postgresConnectionOpts.User,
		postgresConnectionOpts.Host,
		postgresConnectionOpts.Port,
		postgresConnectionOpts.Database,
		postgresConnectionOpts.GetPasswordDefinedStatus(),
	)
	connection, err := connection.NewPostgresConnection(postgresConnectionOpts)
	if err != nil {
		return fmt.Errorf("failed to create a new connection: %s", err)
	}
	log.Info("successfully connected to the database")

	// server instance

	log.Debugf("setting up server instance...")
	serverOpts := server.NewHTTPServerOptions{
		Interface:      conf.GetString(server.ConfigKeyInterface),
		Port:           uint16(conf.GetInt(server.ConfigKeyPort)),
		AllowedMethods: conf.GetStringSlice(server.ConfigKeyCORSAllowedMethods),
		AllowedOrigins: conf.GetStringSlice(server.ConfigKeyCORSAllowedOrigins),
	}
	log.Debugf("allowed methods: ['%s']", strings.Join(serverOpts.AllowedMethods, "', '"))
	log.Debugf("allowed origins: ['%s']", strings.Join(serverOpts.AllowedOrigins, "', '"))
	serverInstance := server.NewHTTPServer(serverOpts)
	serverInstance.ErrorLogger = log.Errorf
	serverInstance.RequestLogger = log.Debugf
	log.Info("successfully instantiated server instance")

	// sessions stuff

	log.Debug("setting up session configuration...")
	sessionOpts := session.GetSignVerifyTokenPairOpts{
		SigningKeyBase64:   conf.GetString(session.ConfigKeySigningKey),
		SigningKeyPath:     conf.GetString(session.ConfigKeySigningKeyPath),
		SigningKeyPassword: conf.GetString(session.ConfigKeySigningKeyPassword),
		VerifyingKeyBase64: conf.GetString(session.ConfigKeyVerifyingKey),
		VerifyingKeyPath:   conf.GetString(session.ConfigKeyVerifyingKeyPath),
	}
	if len(sessionOpts.SigningKeyBase64) > 0 && len(sessionOpts.VerifyingKeyBase64) > 0 {
		log.Debug("registering keys based on base64-encoded strings...")
	} else if len(sessionOpts.SigningKeyPath) > 0 && len(sessionOpts.VerifyingKeyPath) > 0 {
		log.Debugf("registering private/public keys from paths [%s, %s]...", sessionOpts.SigningKeyPath, sessionOpts.VerifyingKeyPath)
	}
	if len(sessionOpts.SigningKeyPassword) > 0 {
		log.Debug("key password was specified")
	}
	privKey, pubKey, err := session.GetSignVerifyTokenPair(sessionOpts)
	if err != nil {
		return fmt.Errorf("failed to evaluate keys for session signing: %s", err)
	}
	serverInstance.CookieID = session.DefaultCookieID
	serverInstance.TokenDataInserter = session.InsertSessionDataIntoRequest
	serverInstance.TokenDataProvider = func(token string) (map[string]interface{}, error) {
		parsedToken, err := session.ParseJwtToken(session.ParseJwtTokenOpts{
			Token:        token,
			VerifyingKey: pubKey,
		})
		if err != nil {
			return nil, fmt.Errorf("failed to verify token: %s", err)
		}
		tokenData, err := parsedToken.AsMap(context.Background())
		return tokenData, nil
	}
	log.Info("successfuly set up session configuration")

	// api routing

	log.Debug("setting up application routes...")
	handler, err := api.GetHttpHandler(api.APIOpts{
		ClientBaseURL:       conf.GetString(client.ConfigKeyClientBaseURL),
		DatabaseConnection:  connection,
		GithubClientID:      conf.GetString(github.ConfigKeyClientID),
		GithubClientSecret:  conf.GetString(github.ConfigKeyClientSecret),
		GithubRedirectURI:   conf.GetString(github.ConfigKeyRedirectURI),
		SessionSigningKey:   *privKey,
		SessionVerifyingKey: *pubKey,
		TelegramToken:       conf.GetString(telegram.ConfigKeyToken),
	})
	if err != nil {
		message := fmt.Sprintf("failed to get the http handler: %s", err)
		log.Error(message)
		return fmt.Errorf(message)
	}
	log.Info("successfully set up application routes")

	log.Debug("setting up utility routes...")
	livenessProbeOpts := healthcheck.NewHttpHealthcheckOpts{
		Path: server.DefaultLivenessProbePath,
	}
	serverInstance.LivenessProbeHandler = healthcheck.NewHttpHealthcheck(livenessProbeOpts)

	readinessProbeOpts := healthcheck.NewHttpHealthcheckOpts{
		Path: server.DefaultReadinessProbePath,
	}
	serverInstance.ReadinessProbeHandler = healthcheck.NewHttpHealthcheck(readinessProbeOpts)

	metricsOpts := metrics.NewHttpPrometheusMetricsOpts{
		Path: server.DefaultMetricsPath,
	}
	serverInstance.MetricsHandler = metrics.NewHttpPrometheusMetrics(metricsOpts)
	log.Infof("successfully set up utility routes at '%s', '%s', and '%s'", livenessProbeOpts.Path, readinessProbeOpts.Path, metricsOpts.Path)

	log.Infof("starting the server on '%s:%v'...", serverOpts.Interface, serverOpts.Port)
	return serverInstance.Start(handler)
}
