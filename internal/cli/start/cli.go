package start

import (
	"github.com/spf13/cobra"
	"gitlab.com/codeprac/codeprac/internal/cli/start/server"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use:     "start",
		Aliases: []string{"s"},
		RunE: func(cmd *cobra.Command, args []string) error {
			return cmd.Help()
		},
	}
	command.AddCommand(server.GetCommand())
	return command
}
