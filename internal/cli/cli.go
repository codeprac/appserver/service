package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/codeprac/codeprac/internal/cli/migrate"
	"gitlab.com/codeprac/codeprac/internal/cli/start"
	"gitlab.com/codeprac/codeprac/internal/constants"
)

func GetCommand() *cobra.Command {
	command := &cobra.Command{
		Use: "codeprac",
		RunE: func(cmd *cobra.Command, args []string) error {
			return cmd.Help()
		},
		Version: constants.Version,
	}
	command.AddCommand(migrate.GetCommand())
	command.AddCommand(start.GetCommand())
	return command
}
