#!/bin/sh
export PGPASSWD="${POSTGRESQL_POSTGRES_PASSWORD}";
psql -U 'postgres' "${POSTGRESQL_DATABASE}" -f '/docker-entrypoint-initdb.d/init.sql';
