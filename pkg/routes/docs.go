/*
	Package routes contains constants that provides paths to
	the webapp service. This needs to be in sync with the
	webbapp service's `./src/data/routes.js`

*/
package routes
