package routes

const (
	Home         = "/"
	Auth         = "/auth"
	Dashboard    = "/dashboard"
	Error        = "/error"
	Gateway      = "/gateway"
	Portfolio    = "/profile/portfolio"
	PortfolioAdd = "/profile/portfolio/add"
	Profile      = "/profile"
	Settings     = "/settings"
	Unauth       = "/unauth"
)
