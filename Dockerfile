FROM golang:1.16-alpine AS build
RUN apk add --no-cache make g++ ca-certificates
WORKDIR /go/src/app
COPY ./go.mod ./go.sum ./
RUN go mod download -x
COPY ./cmd ./cmd
COPY ./internal ./internal
COPY ./pkg ./pkg
COPY ./Makefile ./
# this should be set by the build recipe in the Makefile
ARG RELEASE_TAG=latest
ENV RELEASE_TAG=${RELEASE_TAG}
RUN make release_tag=${RELEASE_TAG} build
RUN mv ./bin/codeprac_$(go env GOOS)_$(go env GOARCH) ./bin/codeprac
RUN mv ./bin/codeprac_$(go env GOOS)_$(go env GOARCH).sha256 ./bin/codeprac.sha256

FROM scratch AS final
COPY --from=build /go/src/app/bin/codeprac /codeprac
COPY --from=build /go/src/app/bin/codeprac.sha256 /codeprac.sha256
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ENTRYPOINT ["/codeprac"]
LABEL repo_url https://gitlab.com/codeprac/appserver/service
LABEL maintainer codeprac
