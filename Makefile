image_registry := registry.gitlab.com
image_owner := codeprac
image_path := appserver/service

-include ./Makefile.properties

image_url ?= $(image_registry)/$(image_owner)/$(image_path)
release_tag ?= latest

# init
setup:
	docker-compose --file ./docker-compose.yml up -d --remove-orphans
keys-session:
	openssl genrsa -out ./.secrets/signing.key 4096
	openssl rsa -in ./.secrets/signing.key -pubout -out ./.secrets/verifying.key
teardown:
	docker-compose --file ./docker-compose.yml down

# develop
migrate-db:
	go run ./cmd/codeprac migrate database
deps:
	go mod vendor
start:
	go run ./cmd/codeprac start server
test:
	mkdir -p ./coverage
	go test -v -mod=mod -covermode=atomic -coverpkg=./... -coverprofile=./coverage/appserver.out ./...
lint:
	go vet ./...
build:
	CGO_ENABLED=0 go build \
		-ldflags " \
			-X gitlab.com/codeprac/codeprac/internal/constants.Version=\"$(release_tag)\" \
			-extldflags 'static' -s -w \
		" \
		-o ./bin/codeprac_$$(go env GOOS)_$$(go env GOARCH) \
		./cmd/codeprac
	cd ./bin && sha256sum codeprac_$$(go env GOOS)_$$(go env GOARCH) > codeprac_$$(go env GOOS)_$$(go env GOARCH).sha256
image:
	docker build \
		--file ./Dockerfile \
		--build-arg release_tag=$(release_tag) \
		--tag $(image_url):latest \
		.
release: image
	docker tag $(image_url):latest $(image_url):$(release_tag)
	docker push $(image_url):$(release_tag)

db-shell:
	PGPASSWORD=password \
	psql \
		--host=localhost \
		--port=5432 \
		--username=user \
		--dbname=database
