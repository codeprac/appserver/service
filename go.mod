module gitlab.com/codeprac/codeprac

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/google/go-github/v36 v36.0.0
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/lestrrat-go/jwx v1.2.2
	github.com/prometheus/client_golang v1.11.0
	github.com/rs/cors v1.8.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	github.com/stretchr/testify v1.7.0
	github.com/usvc/go-config v0.4.1
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.11
)
