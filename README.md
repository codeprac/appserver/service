# Codeprac

[![pipeline status](https://gitlab.com/codeprac/appserver/service/badges/master/pipeline.svg)](https://gitlab.com/codeprac/appserver/service/-/commits/master)
[![coverage report](https://gitlab.com/codeprac/appserver/service/badges/master/coverage.svg)](https://gitlab.com/codeprac/appserver/service/-/commits/master)


Codeprac helps software engineers level-up.

- [Codeprac](#codeprac)
- [Development](#development)
  - [Orientation](#orientation)
  - [Setting up for development](#setting-up-for-development)
    - [Required software](#required-software)
    - [Provisioning the environment](#provisioning-the-environment)
    - [Required configurations](#required-configurations)
    - [Migrating the database](#migrating-the-database)
    - [Starting the app server](#starting-the-app-server)
    - [Starting the web application](#starting-the-web-application)
    - [Starting the content management system](#starting-the-content-management-system)
  - [Other development tasks](#other-development-tasks)
    - [Getting a shell into the datbase](#getting-a-shell-into-the-datbase)
  - [Tearing Down](#tearing-down)
- [Release](#release)
- [Operations](#operations)
- [Licensing](#licensing)

# Development

## Orientation

1. The code is structured around the [Standard Go Project Layout](https://github.com/golang-standards/project-layout)
2. Server-side application/backend
   1. Termed `appserver`
   2. Where the magic happens
   3. Written in Go
3. Web application/frontend
   1. Termed `webapp`
   2. Users access this through a browser
   3. Written in JS (React)
4. Content management site
   1. Termed `cms`
   2. Used for storing practices and articles
   3. This is a Strapi instance
5. Website
   1. Termed `website`
   2. Used for the landing page
   3. This is a Ghost blog instance
6. Documentation can be found in [./docs](./docs)
   1. [Project architecture](./docs/architecture.md)
   2. [Product design](./docs/design.md)
   3. [Technology literature](./docs/literature.md)

## Setting up for development

### Required software

You will need the following software installed for contributing to this project:

1. [Git](https://git-scm.com/) (if it's not obvious)
2. [Go](https://golang.org/doc/install)
3. [Node](https://nodejs.org/en/download/) (personal opinion: install with [`nvm`](https://github.com/nvm-sh/nvm))
4. [Direnv](https://direnv.net/)
5. [Docker](https://docs.docker.com/get-docker/) (for Linux users: install Docker Compose too)

### Provisioning the environment

Run the following to start the development environment:

```sh
make setup;
```

This should bring up:

1. pgAdmin at [http://localhost:5050](http://localhost:5050)
   1. Login with `admin@codepr.ac:password`, the passwords for both databases can be found in the [Docker Compose file](./docker-compose.yml)
2. Postgres database for appserver at [http://localhost:5432](http://localhost:5432)
   1. Login with `PGPASSWORD=password psql -h localhost -p 5432 -U user -d database` from your host machine
3. Strapi at [http://localhost:1337](http://localhost:1337)
   1. Create an account and roll with that
4. Postgres database for Strapi at [http://localhost:5433](http://localhost:5433)
   1. Login with `PGPASSWORD=password psql -h localhost -p 5433 -U user -d database` from your host machine

### Required configurations

See the file at [`./docs/configuration.md`](./docs/configuration.md) and set up an `.envrc` file in the root of this repository with your variables. Get secrets of external services from an existing contributor.

To get the session signing/verifying keys, use `make keys-session`.

You will also need to run `direnv allow` to consume those variables.

### Migrating the database

The database migration is part of the CLI application. To run it:

```sh
make migrate-db;
```

Or manually:

```sh
go run ./cmd/codeprac migrate database;
```

### Starting the app server

> Run `make deps-appserver` to install dependencies

The app server is written in Go. To bring it up, run:

```sh
make start-appserver;
```

Or manually:

```sh
go run ./cmd/codeprac start server;
```

### Starting the web application

> Run `make deps-webapp` to install dependencies

To start the web application locally, run:

```sh
make start-webapp;
```

Or manually:

```sh
cd web && npm start
```

### Starting the content management system

> Run `make install-cms-deps` to install dependencies

To start the Strapi application locally, run:

```sh
make start-cms;
```

Or manually:

```sh
cd cms && npm start
```

## Other development tasks

### Getting a shell into the datbase

You can also administer your local database from [`http://localhost:5050`](http://localhost:5050), but if you really want to create a shell into the database, ensure you have `psql` installed and run:

```sh
make db-shell;
```

## Tearing Down

Run the following to purge the development environment:

```sh
make teardown;
```

# Release

TBD

# Operations

1. `appserver` version can be found by accessing the path `/`
2. `webapp` version can be found by accessing the path at `/version.json`

# Licensing

TBD
