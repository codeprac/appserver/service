# Application Programming Interface

## HTTP-based

| Route                              | Method | Description                                                                                                                  |
| ---------------------------------- | ------ | ---------------------------------------------------------------------------------------------------------------------------- |
| /account                           | GET    | Retrieves the currently authenticated account                                                                                |
| /account                           | PUT    | Updates the currently authenticated account                                                                                  |
| /account/linked                    | GET    | Retrieves linked accounts for the authenticated user                                                                         |
| /account/linked/`{uuid}`/projects  | GET    | Retrieves projects from the linked account with the provided UUID `{uuid}`                                                   |
| /auth/github                       | GET    | Endpoint for performing the OAuth2 flow with Github authentication. Also receives the redirection from Github authentication |
| /profile                           | GET    | Retrieves the profile for the authenticated user                                                                             |
| /profile                           | POST   | Creates a new profile for the authenticated user                                                                             |
| /profile/`{uuid}`                  | PUT    | Updates the profile with UUID `{uuid}`                                                                                       |
| /profile/`{uuid}`/portfolio        | POST   | Adds a portfolio project to the profile with UUID `{uuid}`                                                                   |
| /profile/portfolio                 | POST   | Adds a portfolio project to the primary profile of the authenticated user                                                    |
| /project/github/`{owner}`/`{path}` | GET    | Retrieves the Github repository identified by the Github user `{owner}` and repository name `{path}`                         |
| /project                           | GET    | Retrieves projects belonging to the currently authenticaated user                                                            |
| /session                           | GET    | Retrieves the UUID of the user account if authenticated                                                                      |
| /session/`{token}`                 | GET    | Endpoint that authenticates the user with us using the session `{token}` generated from an OAuth2 flow                       |
