# Tech Stack

## Technologies/Runtimes

- Docker for deployments of bespoke apps
- Go for backend/API/appserver
- JavaScript for frontend/webapp
- [Postgres](https://www.postgresql.org/) for persistent data
- [Strapi](https://strapi.io/) for content management
- [Ghost](https://ghost.org/) for website

## Significant libraries/frameworks

- [gorilla/mux](https://github.com/gorilla/mux)
- [PostCSS](https://postcss.org/)
- [ReactJS](https://reactjs.org/)
- [ReactRouter](https://reactrouter.com/)
- [TailwindCSS](https://tailwindcss.com/)
