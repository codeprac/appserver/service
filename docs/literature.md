# Technology Literature

- [Technology Literature](#technology-literature)
  - [Conventions](#conventions)
    - [Go](#go)
    - [JavaScript](#javascript)
      - [React](#react)

## Conventions

Here are some technologies and their conventions followed (albeit loosely) throughout this project:

### Go

- [Standard Go Project Layout](https://github.com/golang-standards/project-layout)

### JavaScript

#### React

- [Demystifying the Folder Structure of a React App](https://medium.com/swlh/demystifying-the-folder-structure-of-a-react-app-c60b29d90836)
