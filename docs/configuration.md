# Configuration

- [Configuration](#configuration)
  - [Appserver](#appserver)
    - [Required environment variables](#required-environment-variables)
  - [Webapp](#webapp)

## Appserver

To view the full list of configuration values, run `go run ./cmd/codeprac start server --help` in development, or `codeprac start server --help` in production.

### Required environment variables

The following are configurations that should go into your `.envrc` in development:

| Key                           | Description                                                     |
| ----------------------------- | --------------------------------------------------------------- | ---------------------------------------------------- |
| `CLIENT_BASE_URL`             | Base URL for the `appserver` to use to redirect to the `webapp` |
| `GITHUB_CLIENT_ID`            | Client ID for the Github application                            |
| `GITHUB_CLIENT_SECRET`        | Client Secret for the Github application                        |
| `GITHUB_REDIRECT_URI`         | Redirect URI for the Github application                         |
| `DB_HOST`                     | Hostname for the database connection                            |
| `DB_PORT`                     | Port the database connection should use                         |
| `DB_USER`                     | User to use to authenticate with the database                   |
| `DB_PASSWORD`                 | Password to use to authenticate with the database               |
| `DB_NAME`                     | Name of the database                                            |
| `SESSION_SIGNING_KEY_PATH`    | Path to the private key to be used to sign session tokens       |
| `SESSION_VERIFYING_KEY_PATH`  | Path to the public key to be used to sign session tokens        |
| `SERVER_CORS_ALLOWED_ORIGINS` | Comma-delimited list of base URLs to allow for evaluating       | valid URLs for CORS's `Access-Control-Allow-Origins` |
| `TELEGRAM_TOKEN`              | Telegram bot token as provided by the `@BotFather`              |

## Webapp

| Key                         | Description                                              |
| --------------------------- | -------------------------------------------------------- |
| `REACT_APP_SERVER_BASE_URL` | Base URL for the `webapp` to use to call the `appserver` |
