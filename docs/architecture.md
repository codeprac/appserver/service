# Codeprac Architecture

- [Codeprac Architecture](#codeprac-architecture)
  - [Code Architecture](#code-architecture)
  - [Database](#database)
  - [System Infrastructure](#system-infrastructure)

## Code Architecture

![[image](https://app.diagrams.net/#G18mwiREyOPGTUjvcU3bld3_R69nUkwm7s)](./assets/system.png)

> Resource reference: [https://app.diagrams.net/#G18mwiREyOPGTUjvcU3bld3_R69nUkwm7s](https://app.diagrams.net/#G18mwiREyOPGTUjvcU3bld3_R69nUkwm7s)

## Database

![[image](https://dbdiagram.io/d/60dfea9f0b1d8a6d3964f65e)](./assets/database.png)

> Resource reference: [https://dbdiagram.io/d/60dfea9f0b1d8a6d3964f65e](https://dbdiagram.io/d/60dfea9f0b1d8a6d3964f65e)

## System Infrastructure

![[image](https://app.diagrams.net/#G1Ixhl3ccsVXajsq4XgXk_UUep7d4J6clf)](./assets/infrastructure.png)

> Resource reference: [https://app.diagrams.net/#G1Ixhl3ccsVXajsq4XgXk_UUep7d4J6clf](https://app.diagrams.net/#G1Ixhl3ccsVXajsq4XgXk_UUep7d4J6clf)
