package main

import "gitlab.com/codeprac/codeprac/internal/cli"

func main() {
	cli.GetCommand().Execute()
}
